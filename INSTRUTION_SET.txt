** CALLING CONVENTION:
	push first param
	push second param
	...
	push instance pointer
	call method
	
** INSTRUCTIONS:
operations:		[opcode]
ADD				0x20
		- params: a b	return: a + b
SUB				0x21
		- params: a b	return: a - b
MUL				0x22
		- params: a b	return: a * b
DIV				0x23
		- params: a b	return: a / b

instance manipulation:
NEW	 [32bit]	0x40 
		- heap: new class, return: ref to new instance

PUSHNULL 		0x45
		- stack: reference to null constant

stack manipulation:		
STORE [16bit]	0x50
		- store top of the stack to variable #index
		
LOAD [16bit]   	0x51
		- load #index to the top of the stack

PUSH [32bit] 	0x55
		- push 32bit data on top of stack
		
GETFIELD [32bit] 	0x5A
		- read data from member
		- return: refto:member
SETFIELD [32bit]	0x5B
		- set member data
		- params: instance
		
ASET			0x60
		- params: number, instance
		- sets array[(int) number] to instance
AGET			0x61
		- params: number
		- return: array[(int) number]

function calls
CALL [32bit]	0x70
		- calls function
RET				0x71
		- returns from function

SCALL [32bit] 	0x75
		- calls a static function
		
comparisons
CEQ				0x90
		- params: a b  return: (refto:a == refto:b) for userclasses or ( a == b ) for internal classes
CGE				0x91
		- params: a b  return: (refto:a >= refto:b) for userclasses or ( a >= b ) for internal classes
CLE				0x92
		- params: a b  return: (refto:a <= refto:b) for userclasses or ( a <= b ) for internal classes
CGT				0x93
		- params: a b  return: (refto:a > refto:b) for userclasses or ( a > b ) for internal classes		
CLT				0x94
		- params: a b  return: (refto:a < refto:b) for userclasses or ( a < b ) for internal classes
		
jumps
CJMP [32bit][32bit]		0xA0
		- params: a
		- if (a == 1) jump to [EIP+number] else jump to [EIP+number2]
JMP	[32bit]				0xA1
		- jump to [EIP+number]


		
** CLASSFILE:
* number in bigendian

format:
<"MIRUN">
<"CPOOL"> [32bit sizeof cpool]
{ [32bit int] [constant_name] <0x00> } (foreach string constant, classname, functioname)
{ <"CLASS"> [refto:constant_name] [refto:constant_parentname] {
  (<"FUNCTION">|<"STATICFUNCTION">) [refto:constant_name] [16bit parameter count] [16bit local count] [32bit bytecode length] [bytecode]
} (foreach funtion in class) } (foreach class)


cpool names format:
CL::classname
FN::functionname::numberofparams