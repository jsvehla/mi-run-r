
import java.io.IOException;
import java.io.OutputStream;

public class ASTArrayGet extends SimpleNode {

    public ASTArrayGet(int id) {
        super(id);
    }

    public ASTArrayGet(RCompiler p, int id) {
        super(p, id);
    }

    @Override
    public void writeBCIn(OutputStream out) throws IOException {
        if (children != null) {
            for (int i = 0; i < children.length; i++) {
                SimpleNode n = (SimpleNode) children[i];
                if (n != null) {
                    n.writeBCIn(out);
                }
            }
        }
    }

    @Override
    public void writeBCOut(OutputStream out) throws IOException {
        System.out.println("AGET");
        out.write(0x61);
    }
}