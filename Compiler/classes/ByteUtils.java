
import java.io.UnsupportedEncodingException;
import java.util.logging.Level;
import java.util.logging.Logger;

/*
 * To change this template, choose Tools | Templates and open the template in
 * the editor.
 */
/**
 *
 * @author Jan
 */
public class ByteUtils {

    public static byte[] toBytes(int num) {
        byte[] b = new byte[]{
            (byte) (num >> 24),
            (byte) (num >> 16),
            (byte) (num >> 8),
            (byte) num};
        return b;
    }

    public static byte[] toBytes16(int num) {
        byte[] b = new byte[]{
            (byte) (num >> 8),
            (byte) num};
        return b;
    }

    public static byte[] toBytes(short num) {
        byte[] b = new byte[]{
            (byte) (num >> 8),
            (byte) num};
        return b;
    }

    public static byte[] toBytes(String s) {
        try {
            return s.getBytes("US-ASCII");
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(ByteUtils.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
}
