
import java.io.IOException;
import java.io.OutputStream;

public class ASTChar extends SimpleNode {

    private char character;

    public ASTChar(int id) {
        super(id);
    }

    public ASTChar(RCompiler p, int id) {
        super(p, id);
    }

    public void setChar(String str) {
        char[] ch = str.toCharArray();
        this.character = ch[1];
    }

    @Override
    public void writeBCIn(OutputStream out) throws IOException {
        ASTNumberInt nint = new ASTNumberInt(parser, id);
        ASTExpression expr = new ASTExpression(parser, id);

        System.out.println("\t// bc::char shortcut: '" + character + "'");
        nint.setNumber((int) character);

        expr.jjtAddChild(nint, 0);
        expr.writeBCIn(out);
    }
}
