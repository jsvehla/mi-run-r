
import java.io.IOException;
import java.io.OutputStream;

public class ASTBinaryOperator extends SimpleNode {

    enum Operation {

        ADD,
        SUB,
        MUL,
        DIV
    }
    private Operation oper;

    public ASTBinaryOperator(int id) {
        super(id);
    }

    public ASTBinaryOperator(RCompiler p, int id) {
        super(p, id);
    }

    public void setOperation(String op) {
        if (op.equals("+")) {
            oper = Operation.ADD;
        } else if (op.equals("-")) {
            oper = Operation.SUB;
        } else if (op.equals("/")) {
            oper = Operation.DIV;
        } else {
            oper = Operation.MUL;
        }
    }

    public String toString() {
        return RCompilerTreeConstants.jjtNodeName[id] + " " + oper;
    }

    public void writeBCIn(OutputStream out) throws IOException {
        if (children != null) {
            for (int i = 0; i < children.length; ++i) {
                SimpleNode n = (SimpleNode) children[i];
                if (n != null) {
                    n.writeBCIn(out);
                }
            }
        }
        System.out.println(oper.toString());
        byte b[] = new byte[]{};
        switch (oper) {
            case ADD:
                b = new byte[]{0x20};
                break;
            case SUB:
                b = new byte[]{0x21};
                break;
            case MUL:
                b = new byte[]{0x22};
                break;
            case DIV:
                b = new byte[]{0x23};
                break;
        }
        out.write(b);
    }
}