
import java.io.IOException;
import java.io.OutputStream;

public class ASTExpression extends SimpleNode {

    public ASTExpression(int id) {
        super(id);
    }

    public ASTExpression(RCompiler p, int id) {
        super(p, id);
    }

    @Override
    public void writeBCIn(OutputStream out) throws IOException {
        if (children != null) {
            writeBC(out, children.length - 1);
        }
    }

    private void writeBC(OutputStream out, int pos) throws IOException {
        if (pos < 0) {
            return;
        }
        SimpleNode n = (SimpleNode) children[pos];
        if (n != null) {
            n.writeBCIn(out);
        }

        writeBC(out, pos - 1);

        if (n != null) {
            n.writeBCOut(out);
        }
    }
}
