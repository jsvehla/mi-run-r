
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Stack;

public class Compiler {

    private static Compiler instance;
    private Stack<Environment> envs = new Stack<>();
    private Map<String, Integer> constantPool = new HashMap<>();

    private Compiler() {
        envs.push(new Environment(null, ""));
    }

    public static Compiler getInstance() {
        if (instance == null) {
            instance = new Compiler();
        }
        return instance;
    }

    public Environment getCurrent() {
        return envs.peek();
    }

    public void createFunctionEnvironment(String funcId) {
      //  System.out.println("// env::new_environment: " + funcId);
        envs.push(new Environment(getCurrent().getParent(), funcId));
    }

    public void closeEnvironment() {
     //   System.out.println("// env::close_environment");
        envs.pop();
    }

    public int getConstantPoolName(String name){

        if(constantPool.get(name) == null) {
            int pos = constantPool.size();
            constantPool.put(name, pos);

            System.out.println("\t// cp::register_name: " + name + " " + pos);
            return pos;
        }

        return constantPool.get(name);
    }

    public void writeConstantPool(OutputStream out) throws IOException {
        out.write(ByteUtils.toBytes("CPOOL"));

        ByteArrayOutputStream b = new ByteArrayOutputStream();
        for(String s : constantPool.keySet()) {
            int value = constantPool.get(s);

            b.write(ByteUtils.toBytes(value));
            b.write(ByteUtils.toBytes(s));
            b.write(0x00);
        }

        // write size of cpool
        out.write(ByteUtils.toBytes(b.size()));
        b.writeTo(out);
    }
}
