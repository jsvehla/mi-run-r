
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;

public class ASTComparison extends SimpleNode {

    public ASTComparison(int id) {
        super(id);
    }

    public ASTComparison(RCompiler p, int id) {
        super(p, id);
    }

    public void writeBCIn(OutputStream out) throws IOException {
        if (children != null) {
            // write condition
            ((SimpleNode) children[0]).writeBCIn(out);
            System.out.println("cjmp");

            System.out.println("\t// bc::compare true");
            ByteArrayOutputStream jmp = new ByteArrayOutputStream();
            ((SimpleNode) children[1]).writeBCIn(jmp);


            System.out.println("\t// bc::compare else");
            ByteArrayOutputStream ejmp = new ByteArrayOutputStream();
            if (children.length == 3) {
                ((SimpleNode) children[2]).writeBCIn(ejmp);
            }
            System.out.println("\t// bc::compare end");

            out.write((byte) 0xA0);
            // jump to true block
            out.write(ByteUtils.toBytes(0));
            // jump to else block (+5 for generated true-end jump)
            out.write(ByteUtils.toBytes(jmp.size() + 5));
            // write true block
            jmp.writeTo(out);
            // write jmp to skip else block if true
            out.write((byte) 0xA1);
            out.write(ByteUtils.toBytes(ejmp.size()));
            // write else block
            ejmp.writeTo(out);
        }

    }
}