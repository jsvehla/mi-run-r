
import java.io.IOException;
import java.io.OutputStream;

public class ASTStaticCall extends ASTFunctionCall {

    public ASTStaticCall(int id) {
        super(id);
    }

    public ASTStaticCall(RCompiler p, int id) {
        super(p, id);
    }

    @Override
    public void writeBCOut(OutputStream out) throws IOException {
        int length = 0;
        if (children != null) {
            length = (children.length);
        }

        // call
        System.out.println("scall FN::" + functionName + "::" + length);
        int c = Compiler.getInstance().getConstantPoolName("FN::" + functionName + "::" + length);
        out.write(0x75);
        out.write(ByteUtils.toBytes(c));
    }
}