
import java.io.IOException;
import java.io.OutputStream;

public class ASTReassignment extends SimpleNode {

    private String name;

    public ASTReassignment(int id) {
        super(id);
    }

    public ASTReassignment(RCompiler p, int id) {
        super(p, id);
    }

    public void setIdentifier(String name) {
        this.name = name;
    }

    public String toString() {
        return RCompilerTreeConstants.jjtNodeName[id] + " " + name;
    }

    public void writeBCIn(OutputStream out) throws IOException {
        if (children != null) {
            for (int i = 0; i < children.length; ++i) {
                SimpleNode n = (SimpleNode) children[i];
                if (n != null) {
                    n.writeBCIn(out);
                }
            }
        }
        short var = Compiler.getInstance().getCurrent().getLocalAddr(name);
        System.out.println("store " + var);

        out.write(0x50);
        out.write(ByteUtils.toBytes(var));
    }
}