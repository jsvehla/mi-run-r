
import java.io.IOException;
import java.io.OutputStream;

public class ASTMemberAssignment extends SimpleNode {

    private String memberName;

    public ASTMemberAssignment(int id) {
        super(id);
    }

    public ASTMemberAssignment(RCompiler p, int id) {
        super(p, id);
    }

    public void setMemberName(String name) {
        this.memberName = name;
    }

    @Override
    public void writeBCIn(OutputStream out) throws IOException {
        // skip first children - 'self' param
        if (children != null) {
            for (int i = 1; i < children.length; ++i) {
                SimpleNode n = (SimpleNode) children[i];
                if (n != null) {
                    n.writeBCIn(out);
                }
            }

            // push instance
            SimpleNode self = (SimpleNode) children[0];
            self.writeBCIn(out);
        }

        // member assign
        System.out.println("setfield " + memberName);
        int c = Compiler.getInstance().getConstantPoolName(memberName);
        out.write(0x5B);
        out.write(ByteUtils.toBytes(c));
    }
}
