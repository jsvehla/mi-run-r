
import java.io.IOException;
import java.io.OutputStream;

public class ASTArrayAssignment extends SimpleNode {

    public ASTArrayAssignment(int id) {
        super(id);
    }

    public ASTArrayAssignment(RCompiler p, int id) {
        super(p, id);
    }

    @Override
    public void writeBCIn(OutputStream out) throws IOException {
        if (children != null) {
            for (int i = children.length - 1; i >= 0; --i) {
                SimpleNode n = (SimpleNode) children[i];
                if (n != null) {
                    n.writeBCIn(out);
                }
            }
        }

        System.out.println("ASET");
        out.write(0x60);
    }
}