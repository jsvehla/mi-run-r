
import java.io.IOException;
import java.io.OutputStream;

public class ASTNull extends SimpleNode {

    public ASTNull(int id) {
        super(id);
    }

    public ASTNull(RCompiler p, int id) {
        super(p, id);
    }

    public void writeBCIn(OutputStream out) throws IOException {
        if (children != null) {
            for (int i = 0; i < children.length; ++i) {
                SimpleNode n = (SimpleNode) children[i];
                if (n != null) {
                    n.writeBCIn(out);
                }
            }
        }
        out.write(0x45);
    }
}