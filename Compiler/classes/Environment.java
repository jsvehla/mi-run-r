
import java.util.HashMap;
import java.util.Map;

/*
 * To change this template, choose Tools | Templates and open the template in
 * the editor.
 */
/**
 *
 * @author Jan
 */
public class Environment {

    private Environment parent;
    private Map<String, Short> vars = new HashMap<>();
    private String functionId;
    private short paramCnt;

    public Environment(Environment parent, String functionId) {
        this.functionId = functionId;
        this.parent = parent;
        this.paramCnt = 0;
    }

    public Environment getParent() {
        return parent;
    }

    public short registerLocal(String name) {
        if (vars.get(name) != null) {
            throw new RuntimeException("// Error::Value redeclaration: " + functionId + "::" + name);
        }

        short v  = (short) vars.size();

        System.out.println("\t// env::register: " + name + " " + v);
        vars.put(name, v );

        return v;
   }

    public short registerParam(String name) {
        ++paramCnt;
        return registerLocal(name);
    }

    public short getLocalAddr(String name) {
        Short ebp = vars.get(name);
        if (ebp != null) {
            return ebp;
        }

        throw new RuntimeException("// Error::undeclared variable: " + functionId + "::" + name);
    }

    public int getParamCnt() {
        return paramCnt;
    }

    public short getVarsCnt() {
        return (short) vars.size();
    }
}
