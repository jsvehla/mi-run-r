
import java.io.IOException;
import java.io.OutputStream;

public class ASTComparisonOperator extends SimpleNode {

    enum COperation {

        GT,
        EQ,
        LT
    }
    private COperation oper;

    public ASTComparisonOperator(int id) {
        super(id);
    }

    public ASTComparisonOperator(RCompiler p, int id) {
        super(p, id);
    }

    public void setOperation(String op) {
        if (op.equals("<")) {
            oper = COperation.LT;
        } else if (op.equals("==")) {
            oper = COperation.EQ;
        } else {
            oper = COperation.GT;
        }
    }

    public void writeBCIn(OutputStream out) throws IOException {
        if (children != null) {
            for (int i = 0; i < children.length; ++i) {
                SimpleNode n = (SimpleNode) children[i];
                if (n != null) {
                    n.writeBCIn(out);
                }
            }
        }
        System.out.println(oper.toString());
        byte b[] = new byte[]{};
        switch (oper) {
            case EQ:
                b = new byte[]{(byte) 0x90};
                break;
            case GT:
                b = new byte[]{(byte) 0x93};
                break;
            case LT:
                b = new byte[]{(byte) 0x94};
                break;
        }
        out.write(b);
    }
}