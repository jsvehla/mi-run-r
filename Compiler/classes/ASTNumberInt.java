
import java.io.IOException;
import java.io.OutputStream;

public class ASTNumberInt extends SimpleNode {

    int number;

    public ASTNumberInt(int id) {
        super(id);
    }

    public ASTNumberInt(RCompiler p, int id) {
        super(p, id);
    }

    public void setNumber(int i) {
        number = i;
    }

    public void setNumber(String str) {
        number = Integer.valueOf(str);
    }

    @Override
    public String toString() {
        return RCompilerTreeConstants.jjtNodeName[id] + " " + number;
    }

    @Override
    public void writeBCIn(OutputStream out) throws IOException {
        System.out.println("push " + number);
        out.write(0x55);
        out.write(ByteUtils.toBytes(number));

        System.out.println("new CL::Integer");
        out.write(0x40);
        int c = Compiler.getInstance().getConstantPoolName("CL::Integer");
        out.write(ByteUtils.toBytes(c));

        System.out.println("call FN::Integer::2");
        out.write(0x70);
        c = Compiler.getInstance().getConstantPoolName("FN::Integer::2");
        out.write(ByteUtils.toBytes(c));
    }
}
