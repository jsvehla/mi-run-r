
import java.io.IOException;
import java.io.OutputStream;

public class ASTReturn extends SimpleNode {

    public ASTReturn(int id) {
        super(id);
    }

    public ASTReturn(RCompiler p, int id) {
        super(p, id);
    }

    public void writeBCIn(OutputStream out) throws IOException {
        if (children != null) {
            for (int i = 0; i < children.length; ++i) {
                SimpleNode n = (SimpleNode) children[i];
                if (n != null) {
                    n.writeBCIn(out);
                }
            }
        }
        System.out.println("ret");
        out.write(0x71);
    }
}