
import java.io.IOException;
import java.io.OutputStream;

public class ASTMemberGet extends SimpleNode {

    private String memberName;

    public ASTMemberGet(int id) {
        super(id);
    }

    public ASTMemberGet(RCompiler p, int id) {
        super(p, id);
    }

    public void setMemberName(String name) {
        this.memberName = name;
    }

    @Override
    public void writeBCOut(OutputStream out) throws IOException {
        // member assign
        System.out.println("getfield " + memberName);
        int c = Compiler.getInstance().getConstantPoolName(memberName);
        out.write(0x5A);
        out.write(ByteUtils.toBytes(c));
    }
}