
import java.io.OutputStream;
import java.io.IOException;

public class ASTAssignment extends SimpleNode {

    private String name;

    public ASTAssignment(int id) {
        super(id);
    }

    public ASTAssignment(RCompiler p, int id) {
        super(p, id);
    }

    public void setIdentifier(String name) {
        this.name = name;
    }

    public String toString() {
        return RCompilerTreeConstants.jjtNodeName[id] + " " + name;
    }

    public void writeBCIn(OutputStream out) throws IOException {
        if (children != null) {
            for (int i = 0; i < children.length; ++i) {
                SimpleNode n = (SimpleNode) children[i];
                if (n != null) {
                    n.writeBCIn(out);
                }
            }
        }

        short var = Compiler.getInstance().getCurrent().registerLocal(name);
        System.out.println("store " + var);

        out.write(0x50);
        out.write(ByteUtils.toBytes(var));
    }
}