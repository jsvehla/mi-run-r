
import java.io.IOException;
import java.io.OutputStream;

public class ASTNumberFloat extends SimpleNode {

    float number;

    public ASTNumberFloat(int id) {
        super(id);
    }

    public ASTNumberFloat(RCompiler p, int id) {
        super(p, id);
    }

    public void setNumber(String str) {
        number = Float.valueOf(str);
    }

    @Override
    public String toString() {
        return RCompilerTreeConstants.jjtNodeName[id] + " " + number;
    }

    @Override
    public void writeBCIn(OutputStream out) throws IOException {
        System.out.println("push " + number);
        out.write(0x55);
        out.write(ByteUtils.toBytes(Float.floatToIntBits(number)));

        System.out.println("new CL::Float");
        out.write(0x40);
        int c = Compiler.getInstance().getConstantPoolName("CL::Float");
        out.write(ByteUtils.toBytes(c));

        System.out.println("new FN::Float::2");
        out.write(0x70);
        c = Compiler.getInstance().getConstantPoolName("FN::Float::2");
        out.write(ByteUtils.toBytes(c));
    }
}
