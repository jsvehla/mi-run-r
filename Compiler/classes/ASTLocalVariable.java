
import java.io.IOException;
import java.io.OutputStream;

public class ASTLocalVariable extends SimpleNode {

    private String name;

    public ASTLocalVariable(int id) {
        super(id);
    }

    public ASTLocalVariable(RCompiler p, int id) {
        super(p, id);
    }

    public void setName(String name) {
        this.name = name;
    }

    public String toString() {
        return RCompilerTreeConstants.jjtNodeName[id] + " " + name;
    }

    public void writeBCIn(OutputStream out) throws IOException {
        short var = Compiler.getInstance().getCurrent().getLocalAddr(name);
        System.out.println("load " + var);
        out.write(0x51);
        out.write(ByteUtils.toBytes(var));
    }
}