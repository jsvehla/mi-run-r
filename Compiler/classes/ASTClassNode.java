
import java.io.IOException;
import java.io.OutputStream;

public class ASTClassNode extends SimpleNode {

    private String className;
    private String parentName;

    public ASTClassNode(int id) {
        super(id);
    }

    public ASTClassNode(RCompiler p, int id) {
        super(p, id);
    }

    public void setClassName(String name) {
        className = name;
    }

    public void setParentName(String name) {
        parentName = name;
    }

    public String toString() {
        return RCompilerTreeConstants.jjtNodeName[id] + " " + className;
    }

    public void writeBCIn(OutputStream out) throws IOException {
        // class header
        String s = "CLASS";
        out.write(ByteUtils.toBytes(s));
        System.out.println("\t// bc::class_start " + className + ", parent " + parentName);

        // name
        int c = Compiler.getInstance().getConstantPoolName("CL::" + className);
        out.write(ByteUtils.toBytes(c));

        // parent name
        c = Compiler.getInstance().getConstantPoolName("CL::" + parentName);
        out.write(ByteUtils.toBytes(c));

        if (children != null) {
            for (int i = 0; i < children.length; ++i) {
                SimpleNode n = (SimpleNode) children[i];
                if (n != null) {
                    n.writeBCIn(out);
                }
            }
        }
        System.out.println("\t// bc::class_end");
    }
}