
import java.io.IOException;
import java.io.OutputStream;

public class ASTFunctionVariable extends SimpleNode {

    private String name;

    public ASTFunctionVariable(int id) {
        super(id);
    }

    public ASTFunctionVariable(RCompiler p, int id) {
        super(p, id);
    }

    public void setName(String name) {
        this.name = name;
    }

    public String toString() {
        return RCompilerTreeConstants.jjtNodeName[id] + " " + name;
    }

    public void writeBCIn(OutputStream out) throws IOException {
        Compiler.getInstance().getCurrent().registerParam(name);
    }
}
