package GarbageCollector;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class GTree extends GNode {

    private Map<Integer, GNode> nodes;

    public GTree(Map<Integer, GNode> nodes) {
        super(-1);
        this.nodes = nodes;
    }

    public List<Integer> objectsToDelete() {
        List<Integer> toReturn = new ArrayList<Integer>();
        DFS(this);

        for (Integer id : nodes.keySet()) {
            if (nodes.get(id).isSigned() == false) {
                toReturn.add(id);
            }
        }

        return toReturn;
    }

    public void DFS(GNode parent) {
        parent.visited();
        for (GNode child : parent.getChilds()) {
            if (child.isSigned() == false) {
                DFS(child);
            }
        }
    }
}
