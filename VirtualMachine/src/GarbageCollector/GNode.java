package GarbageCollector;

import java.util.HashSet;
import java.util.Set;

public class GNode {

    private Set<GNode> childs;
    private int id;
    boolean signed;

    public GNode(int id) {
        childs = new HashSet<GNode>();
        signed = false;
        this.id = id;
    }

    public boolean addChild(GNode child) {
        if (childs.contains(child) == false) {
            childs.add(child);
            return true;
        }
        return false;
    }

    public int getId() {
        return id;
    }

    public Set<GNode> getChilds() {
        return childs;
    }

    public boolean isSigned() {
        return signed;
    }

    public void visited() {
        signed = true;
    }
}