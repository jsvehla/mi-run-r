package GarbageCollector;

import java.util.*;
import virtualmachine.execution.ExecFrame;
import virtualmachine.execution.Heap;
import virtualmachine.metadata.RInstance;
import virtualmachine.metadata.internal.RInstanceNULL;
import virtualmachine.reference.RReference;

public class GarbageCollector {

    private Heap heap;
    private GTree gTree;
    private Map<Integer, GNode> nodes;

    public GarbageCollector(Heap heap, Stack<ExecFrame> execStack, Stack<RReference> dataStack) {
        this.heap = heap;
        nodes = new HashMap<Integer, GNode>();
        this.gTree = new GTree(nodes);
        Iterator<Integer> it = heap.getIterator();
        while (it.hasNext()) {
            int value = it.next();
            if (value != 0) { //NULL instance
                nodes.put(value, new GNode(value));
            }
        }

        for (RReference i : dataStack) {
            if (i.isHeapPointer() && (i.getInstance() != RInstanceNULL.getInstance())) {
                if (heap.get(i.getData()) != null) {
                    GNode node = new GNode(i.getData());
                    nodes.put(i.getData(), node);
                    gTree.addChild(node);
                }
            }
        }

        for (ExecFrame e : execStack) {
            int varCount = e.getCurrentMethod().getVarCount();
            for (int i = 0; i < varCount; i++) {
                if ((e.loadVar(i) != null) && (e.loadVar(i).isHeapPointer()) && (e.loadVar(i).getInstance() != RInstanceNULL.getInstance())) {
                    gTree.addChild(nodes.get(e.loadVar(i).getData()));
                }
            }
        }

        createTree();
    }

    private void createTree() {
        for (GNode g : gTree.getChilds()) {
            DFS(g);
        }
    }

    private void DFS(GNode gNode) {
        RInstance rInstance = heap.get(gNode.getId());
        Iterator<String> it = rInstance.getMembersIterator();
        while (it.hasNext()) {
            RReference heapPos = rInstance.getMember(it.next());
            if (heapPos.getInstance() != RInstanceNULL.getInstance()) {
                GNode node = nodes.get(heapPos.getData());
                if (gNode.addChild(node)) {
                    DFS(node);
                }
            }
        }
    }

    public void clean() {
        List<Integer> objectsToDelete = gTree.objectsToDelete();
        for (Integer id : objectsToDelete) {
            heap.remove(id);
        }
    }
}
