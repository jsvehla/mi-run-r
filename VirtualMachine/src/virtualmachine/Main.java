package virtualmachine;

public class Main {

    public static void main(String[] args) {
        VirtualMachine vm = new VirtualMachine();
        String params = "";
        for (int i = 1; i < args.length; i++) {
            params += args[i];
            if (i < args.length - 1) {
                params += " ";
            }
        }
        vm.loadProgram(args[0], params);
        vm.run();
    }
}
