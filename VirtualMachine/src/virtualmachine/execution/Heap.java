package virtualmachine.execution;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import virtualmachine.metadata.RInstance;
import virtualmachine.metadata.internal.RInstanceNULL;

public class Heap {

    private Map<Integer, RInstance> heap;
    private int lastRef = 0;
    private int nullRef = 0;

    public Heap() {
        heap = new HashMap<Integer, RInstance>();
        heap.put(0, RInstanceNULL.getInstance());
    }

    public Iterator<Integer> getIterator() {
        return heap.keySet().iterator();
    }

    public void add(int key, RInstance instance) {
        heap.put(key, instance);
    }

    public void remove(int key) {
        heap.remove(key);
    }

    public int add(RInstance instance) {
        if (heap.size() == Integer.MAX_VALUE) {
            throw new RuntimeException("Heap is full.");
        }
        while (heap.get(lastRef) != null) {
            lastRef += 1;
        }
        heap.put(lastRef, instance);
        return lastRef;
    }

    public RInstance get(int key) {
        return heap.get(key);
    }

    public int getNullRef() {
        return nullRef;
    }

    public int size() {
        return heap.size();
    }
}
