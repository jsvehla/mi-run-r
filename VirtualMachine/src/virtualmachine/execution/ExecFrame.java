package virtualmachine.execution;

import virtualmachine.metadata.RMethod;
import virtualmachine.reference.RReference;

public class ExecFrame {

    private int EIP;
    private int EBP;
    private RMethod currentMethod;
    private RReference[] vars;
    private int lastStored;

    public ExecFrame(int EBP, RMethod currentMethod) {
        this.EIP = 0;
        this.EBP = EBP;
        this.currentMethod = currentMethod;
        this.vars = new RReference[currentMethod.getVarCount()];
        this.lastStored = -1;
    }

    public ExecFrame(RMethod currentMethod) {
        this(0, currentMethod);
    }

    public void setEBP(int EBP) {
        this.EBP = EBP;
    }

    public void moveEIP(int i) {
        EIP += i;
    }

    public int getEIP() {
        return EIP;
    }

    public int getEBP() {
        return EBP;
    }

    public RMethod getCurrentMethod() {
        return currentMethod;
    }

    public void storeVar(int pos, RReference var) {
        vars[pos] = var;
    }

    public RReference loadVar(int pos) {
        return vars[pos];
    }

    public boolean doesVarExist(int value) {
        if ((value < 0) || (value >= vars.length)) {
            return false;
        }
        return true;
    }

    public byte[] getCurrentBC() {
        return currentMethod.getBC();
    }
}
