package virtualmachine.execution;

import java.nio.ByteBuffer;
import virtualmachine.metadata.internal.RInstanceArray;
import virtualmachine.metadata.internal.RInstanceInteger;
import virtualmachine.reference.RReferenceHeap;

public class ByteCodeOperations {

    public static String beginning = "MIRUN";
    public static String afterBeginning = "CPOOL";
    public static String className = "CLASS";
    public static String methodName = "FUNCTION";
    public static String methodStaticName = "STATICFUNCTION";

    public static RInstanceArray stringToRArray(String text, Heap heap){
            RInstanceArray array = new RInstanceArray();
            array.setSize(text.length());
            for (int i = 0; i < text.length(); i++) {
                RInstanceInteger rInt = new RInstanceInteger(text.charAt(i));
                int heapPos = heap.add(rInt);
                array.setIndex(i, new RReferenceHeap(heap, heapPos));
            }
            return array;
    }

    public static int getParam32(byte[] bc, int pos) {
        ByteBuffer bb = ByteBuffer.wrap(bc);
        return bb.getInt(pos);
    }

    public static short getParam16(byte[] bc, int pos) {
        ByteBuffer bb = ByteBuffer.wrap(bc);
        return bb.getShort(pos);
    }

    /**
     * Reads the string in bytecode. The string is ended with 0x00
     *
     * @param bc bytecode
     * @param pos position of first letter
     * @return
     */
    public static String readString(byte[] bc, int pos) {
        ByteBuffer bb = ByteBuffer.wrap(bc);
        String toReturn = "";
        while (bb.get(pos) != 0x00) {
            toReturn += (char) bb.get(pos);
            pos++;
        }
        return toReturn;
    }

    public static boolean sameStrings(byte[] bc, String text, int pos) {
        for (int i = 0; i < text.length(); i++) {
            if (text.charAt(i) != (char) bc[pos + i]) {
                return false;
            }
        }
        return true;
    }
}
