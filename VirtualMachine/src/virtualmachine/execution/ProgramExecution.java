package virtualmachine.execution;

import GarbageCollector.GarbageCollector;
import java.util.Stack;
import virtualmachine.Loadable;
import virtualmachine.instructions.*;
import virtualmachine.loading.ClassPool;
import virtualmachine.loading.ConstantPool;
import virtualmachine.metadata.RClass;
import virtualmachine.metadata.RMethod;
import virtualmachine.metadata.internal.*;
import virtualmachine.metadata.internal.method.RConstructorArray;
import virtualmachine.metadata.internal.method.RConstructorFloat;
import virtualmachine.metadata.internal.method.RConstructorInteger;
import virtualmachine.nativelibraries.CLStatic;
import virtualmachine.reference.RReference;
import virtualmachine.reference.RReferenceHeap;

public class ProgramExecution {

    private ClassPool classPool;
    private ConstantPool constantPool;
    private Stack<RReference> dataStack;
    private Stack<ExecFrame> execStack;
    private Heap heap;
    private int paramsPosition;
    private Loadable virtualMachine;
    private boolean stopInstructions;
    //Garbage Collector resources
    private int garbageCounter;
    private int lastSize;

    public ProgramExecution(String params, Loadable virtualMachine) {
        dataStack = new Stack<RReference>();
        execStack = new Stack<ExecFrame>();
        classPool = new ClassPool();
        constantPool = new ConstantPool();
        heap = new Heap();
        this.paramsPosition = heap.add(ByteCodeOperations.stringToRArray(params, heap));
        prepareInternalMetadata();
        this.virtualMachine = virtualMachine;
        this.stopInstructions = true;
        this.garbageCounter = 0;
        this.lastSize = 0;
    }

    public void breakProcess() {
        this.stopInstructions = true;
    }

    public void checkGC() {

        int howMuch = heap.size() - lastSize;
        if (heap.size() - lastSize > 0) {
            garbageCounter += howMuch;
        }
        lastSize = heap.size();

        if (garbageCounter >= 10000) {
            GarbageCollector gc = new GarbageCollector(heap, execStack, dataStack);
            gc.clean();
            garbageCounter = 0;
        }
    }

    private void prepareInternalMetadata() {
        // Base class
        RClassBase CBase = RClassBase.getInstance();
        classPool.putRClass(CBase);

        // integer
        RClassInteger CInt = RClassInteger.getInstance();
        CInt.addMethod(new RConstructorInteger());
        classPool.putRClass(CInt);

        // float
        RClassFloat CFl = RClassFloat.getInstance();
        CFl.addMethod(new RConstructorFloat());
        classPool.putRClass(CFl);

        // array
        RClassArray CArray = RClassArray.getInstance();
        CArray.addMethod(new RConstructorArray());
        classPool.putRClass(CArray);

        // null
        classPool.putRClass(RClassNull.getInstance());

        //native libraries
        RClass staticRClass = CLStatic.getInstance();
        classPool.putRClass(staticRClass);
    }

    public ClassPool getClassPool() {
        return classPool;
    }

    public ConstantPool getConstantPool() {
        return constantPool;
    }

    public void setEntryPoint(RMethod main) {
        ExecFrame f = new ExecFrame(dataStack.size(), main);
        f.storeVar(0, new RReferenceHeap(heap, this.paramsPosition));
        execStack.push(f);
    }

    public boolean runEntryPoint() {
        RInstruction inst;
        while ((!execStack.isEmpty())) {
            // try GC
            checkGC();
            // fetch step
            inst = fetch();
            // execute step
            execute(inst);

            if (this.stopInstructions) {
                return false;
            }
        }
        return execStack.isEmpty();
    }

    public RInstruction fetch() {
        // read instruction from bytecode
        int eip = execStack.peek().getEIP();
        byte[] bc = execStack.peek().getCurrentMethod().getBC();
        byte inst = bc[eip];

        // increase eip for read instuction code
        execStack.peek().moveEIP(1);

        switch (inst) {
            case 0x20:
                return new RInstructionADD(dataStack, execStack, heap);
            case 0x21:
                return new RInstructionSUB(dataStack, execStack, heap);
            case 0x22:
                return new RInstructionMUL(dataStack, execStack, heap);
            case 0x23:
                return new RInstructionDIV(dataStack, execStack, heap);
            case 0x40:
                return new RInstructionNEW(constantPool, classPool, dataStack, execStack, heap);
            case 0x45:
                return new RInstructionPUSHNULL(dataStack, execStack, heap);
            case 0x50:
                return new RInstructionSTORE(dataStack, execStack, heap);
            case 0x51:
                return new RInstructionLOAD(dataStack, execStack, heap);
            case 0x55:
                return new RInstructionPUSH(dataStack, execStack, heap);
            case 0x5A:
                return new RInstructionGETFIELD(dataStack, execStack, heap, constantPool);
            case 0x5B:
                return new RInstructionSETFIELD(dataStack, execStack, heap, constantPool);
            case 0x60:
                return new RInstructionASET(dataStack, execStack, heap);
            case 0x61:
                return new RInstructionAGET(dataStack, execStack, heap);
            case 0x70:
                return new RInstructionCALL(constantPool, dataStack, execStack, heap, virtualMachine);
            case 0x71:
                return new RInstructionRET(dataStack, execStack, heap);
            case 0x75:
                return new RInstructionSCALL(constantPool, classPool, dataStack, execStack, heap, virtualMachine);
            case (byte) 0x90:
                return new RInstructionCEQ(dataStack, execStack, heap);
            case (byte) 0x93:
                return new RInstructionCGT(dataStack, execStack, heap);
            case (byte) 0xA0:
                return new RInstructionCJMP(dataStack, execStack, heap);
            case (byte) 0xA1:
                return new RInstructionJMP(dataStack, execStack, heap);
        }
        return null;
    }

    public void execute(RInstruction inst) {
        try {
            inst.execute();
        } catch (Exception e) {
            // TODO: translate java excetion to program exception and throw it.
            // To Be Implemented - exception handling:
            // Basic idea inspired by SEH.
            // All exception handlers are descendants of ExceptionHandler with method handle(var obj)
            // Each exec frame has its own exception handler (user specified or default).
            // When exception in frame (method) occurs, frame is popped and VM calls handle()
            // from popped Ex.Handler with exception parameters. There, exception is handled either by
            // doing something (recognition by error codes?) or rethrowing it to upper frame (default action)
            // repeating this until it's handled or it reaches the top.

            // Clear stack to exit program
            execStack.clear();
            System.err.println("The program swallowed a bug and had to be exited. Printing an error message.");
            e.printStackTrace();
            System.err.println("End of error message, enjoy your day.");
        }
    }
}
