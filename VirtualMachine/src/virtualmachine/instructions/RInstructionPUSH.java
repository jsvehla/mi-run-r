package virtualmachine.instructions;

import java.util.Stack;
import virtualmachine.execution.ByteCodeOperations;
import virtualmachine.execution.ExecFrame;
import virtualmachine.execution.Heap;
import virtualmachine.reference.RReference;
import virtualmachine.reference.RReferenceData;

public class RInstructionPUSH extends RInstruction {

    public RInstructionPUSH(Stack<RReference> dataStack, Stack<ExecFrame> execStack, Heap heap) {
        super(dataStack, execStack, heap);
    }

    @Override
    public void execute() {
        // load param
        byte[] bc = execStack.peek().getCurrentBC();
        int eip = execStack.peek().getEIP();
        int val = ByteCodeOperations.getParam32(bc, eip);

        // push raw data
        dataStack.add(new RReferenceData(heap, val));

        // move eip for 32bit integer
        execStack.peek().moveEIP(4);
    }
}
