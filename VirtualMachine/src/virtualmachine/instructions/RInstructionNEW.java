package virtualmachine.instructions;

import java.util.Stack;
import virtualmachine.execution.ByteCodeOperations;
import virtualmachine.execution.ExecFrame;
import virtualmachine.execution.Heap;
import virtualmachine.loading.ClassPool;
import virtualmachine.loading.ConstantPool;
import virtualmachine.metadata.RClass;
import virtualmachine.metadata.RInstance;
import virtualmachine.reference.RReference;
import virtualmachine.reference.RReferenceHeap;

public class RInstructionNEW extends RInstruction {

    private ConstantPool constantPool;
    private ClassPool classPool;

    public RInstructionNEW(ConstantPool constantPool, ClassPool classPool, Stack<RReference> dataStack, Stack<ExecFrame> execStack, Heap heap) {
        super(dataStack, execStack, heap);
        this.constantPool = constantPool;
        this.classPool = classPool;
    }

    @Override
    public void execute() {
        // idea is: look into class pool for class and create new uninitialized instance
        // then call constructor later in the bytecode
        // for intenral types, I set up internal constructors.
        // CALL instruction will invoke RMethod.call(...) before doing anything.
        // If it's user method it will not do anything, but if it's internal method it will do java code (ex. load file)
        // same mechanism is used for constructors of internal types

        // get classname from parameter
        byte[] bc = execStack.peek().getCurrentBC();
        int cl = ByteCodeOperations.getParam32(bc, execStack.peek().getEIP());
        execStack.peek().moveEIP(4);

        // load class and push new instance
        String cls = constantPool.get(cl);
        RClass rcl = classPool.getRClassByName(cls);
        if (rcl == null) {
            throw new RuntimeException("Unknown class");
        }
        RInstance inst = rcl.newInstance();
        dataStack.add(new RReferenceHeap(heap, heap.add(inst)));
    }
}
