package virtualmachine.instructions;

import java.util.Stack;
import virtualmachine.execution.ExecFrame;
import virtualmachine.execution.Heap;
import virtualmachine.metadata.RInstance;
import virtualmachine.metadata.internal.RInstanceArray;
import virtualmachine.reference.RReference;

public class RInstructionAGET extends RInstruction {

    public RInstructionAGET(Stack<RReference> dataStack, Stack<ExecFrame> execStack, Heap heap) {
        super(dataStack, execStack, heap);
    }

    @Override
    public void execute() {
        RReference aref = dataStack.pop();
        RReference iref = dataStack.pop();

        RInstanceArray arr = (RInstanceArray) aref.getInstance();
        RInstance index = iref.getInstance();

        dataStack.push(arr.getIndex(index.toInt()));
    }
}
