package virtualmachine.instructions;

import java.util.Stack;
import virtualmachine.execution.ByteCodeOperations;
import virtualmachine.execution.ExecFrame;
import virtualmachine.execution.Heap;
import virtualmachine.reference.RReference;

public class RInstructionSTORE extends RInstruction {

    public RInstructionSTORE(Stack<RReference> dataStack, Stack<ExecFrame> execStack, Heap heap) {
        super(dataStack, execStack, heap);
    }

    @Override
    public void execute() {
        byte[] bc = execStack.peek().getCurrentBC();
        ExecFrame exec = execStack.peek();
        int eip = execStack.peek().getEIP();
        int a = ByteCodeOperations.getParam16(bc, eip);
        execStack.peek().moveEIP(2);
        exec.storeVar(a, dataStack.pop());

        // move eip for 16bit integer
    }
}
