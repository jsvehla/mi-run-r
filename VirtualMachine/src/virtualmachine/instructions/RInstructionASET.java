package virtualmachine.instructions;

import java.util.Stack;
import virtualmachine.execution.ExecFrame;
import virtualmachine.execution.Heap;
import virtualmachine.metadata.RInstance;
import virtualmachine.metadata.internal.RInstanceArray;
import virtualmachine.reference.RReference;

public class RInstructionASET extends RInstruction {

    public RInstructionASET(Stack<RReference> dataStack, Stack<ExecFrame> execStack, Heap heap) {
        super(dataStack, execStack, heap);
    }

    @Override
    public void execute() {
        RReference aref = dataStack.pop();
        RReference iref = dataStack.pop();
        RReference valref = dataStack.pop();

        RInstanceArray arr = (RInstanceArray) aref.getInstance();
        RInstance index = iref.getInstance();
        arr.setIndex(index.toInt(), valref);

        dataStack.push(aref);
    }
}
