package virtualmachine.instructions;

import java.util.Stack;
import virtualmachine.execution.ExecFrame;
import virtualmachine.execution.Heap;
import virtualmachine.metadata.RInstance;
import virtualmachine.metadata.internal.RClassInteger;
import virtualmachine.metadata.internal.RInstanceInteger;
import virtualmachine.reference.RReference;
import virtualmachine.reference.RReferenceHeap;

public class RInstructionCGT extends RInstruction {

    public RInstructionCGT(Stack<RReference> dataStack, Stack<ExecFrame> execStack, Heap heap) {
        super(dataStack, execStack, heap);
    }

    @Override
    public void execute() {
        RReference aref = dataStack.pop();
        RReference bref = dataStack.pop();

        RInstance inst1 = aref.getInstance();
        RInstance inst2 = bref.getInstance();

        // prepare result
        RInstance res = RClassInteger.getInstance().newInstance();
        RReference resref = new RReferenceHeap(heap, heap.add(res));

        // if nonnative ceq is false, ((refto1 == refto2) || false) is same as (refto1 == refto2)
        // if native (refto1 == refto2) id usually false -> uses ceq to decide
        if (inst2.cgt(inst1)) {
            ((RInstanceInteger) res).setValue(1);
        } else {
            ((RInstanceInteger) res).setValue(0);
        }

        // push result
        dataStack.push(resref);
    }


}
