package virtualmachine.instructions;

import java.util.Stack;
import virtualmachine.execution.ExecFrame;
import virtualmachine.execution.Heap;
import virtualmachine.reference.RReference;

public abstract class RInstruction {

    protected Stack<RReference> dataStack;
    protected Stack<ExecFrame> execStack;
    protected Heap heap;

    public RInstruction(Stack<RReference> dataStack, Stack<ExecFrame> execStack, Heap heap) {
        this.dataStack = dataStack;
        this.execStack = execStack;
        this.heap = heap;
    }

    public abstract void execute();
}
