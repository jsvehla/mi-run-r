package virtualmachine.instructions;

import java.util.Stack;
import virtualmachine.Loadable;
import virtualmachine.execution.ByteCodeOperations;
import virtualmachine.execution.ExecFrame;
import virtualmachine.execution.Heap;
import virtualmachine.loading.ClassPool;
import virtualmachine.loading.ConstantPool;
import virtualmachine.metadata.RClass;
import virtualmachine.metadata.RMethod;
import virtualmachine.reference.RReference;

public class RInstructionSCALL extends RInstruction {

    private ConstantPool constantPool;
    private ClassPool classPool;
    private Loadable virtualMachine;

    public RInstructionSCALL(ConstantPool constantPool, ClassPool classPool, Stack<RReference> dataStack, Stack<ExecFrame> execStack, Heap heap, Loadable virtualMachine) {
        super(dataStack, execStack, heap);
        this.constantPool = constantPool;
        this.classPool = classPool;
        this.virtualMachine = virtualMachine;
    }

    @Override
    public void execute() {
        // get methodname from parameter
        byte[] bc = execStack.peek().getCurrentBC();
        int mi = ByteCodeOperations.getParam32(bc, execStack.peek().getEIP());
        execStack.peek().moveEIP(4);

        // load method name
        String mstr = constantPool.get(mi);

        // lookup method. All static methods are stored in CL::Static group and lookup is not dependent on instance
        RClass rcl = classPool.getRClassByName("CL::Static");
        RMethod m = rcl.getMethod(mstr);

        // call method
        m.call(dataStack, execStack, heap, virtualMachine);
    }
}
