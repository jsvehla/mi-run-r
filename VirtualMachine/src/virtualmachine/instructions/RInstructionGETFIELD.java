package virtualmachine.instructions;

import java.util.Stack;
import virtualmachine.execution.ByteCodeOperations;
import virtualmachine.execution.ExecFrame;
import virtualmachine.execution.Heap;
import virtualmachine.loading.ConstantPool;
import virtualmachine.metadata.RInstance;
import virtualmachine.reference.RReference;
import virtualmachine.reference.RReferenceHeap;

public class RInstructionGETFIELD extends RInstruction {

    private ConstantPool constantPool;

    public RInstructionGETFIELD(Stack<RReference> dataStack, Stack<ExecFrame> execStack, Heap heap, ConstantPool constantPool) {
        super(dataStack, execStack, heap);
        this.constantPool = constantPool;
    }

    @Override
    public void execute() {
        // get membername from parameter
        byte[] bc = execStack.peek().getCurrentBC();
        int mi = ByteCodeOperations.getParam32(bc, execStack.peek().getEIP());
        execStack.peek().moveEIP(4);
        String mstr = constantPool.get(mi);

        // get stack data
        RInstance inst = dataStack.pop().getInstance();

        // get field
        RReference mem = inst.getMember(mstr);
        if (mem == null) {
            mem = new RReferenceHeap(heap, heap.getNullRef());
        }
        dataStack.push(mem);
    }
}
