package virtualmachine.instructions;

import java.util.Stack;
import virtualmachine.execution.ExecFrame;
import virtualmachine.execution.Heap;
import virtualmachine.reference.RReference;
import virtualmachine.reference.RReferenceHeap;


public class RInstructionPUSHNULL extends RInstruction {

    public RInstructionPUSHNULL(Stack<RReference> dataStack, Stack<ExecFrame> execStack, Heap heap) {
        super(dataStack, execStack, heap);
    }

    @Override
    public void execute() {
        int n = heap.getNullRef();

        dataStack.push(new RReferenceHeap(heap, n));
    }
}
