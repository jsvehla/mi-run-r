package virtualmachine.instructions;

import java.util.Stack;
import virtualmachine.execution.ByteCodeOperations;
import virtualmachine.execution.ExecFrame;
import virtualmachine.execution.Heap;
import virtualmachine.loading.ConstantPool;
import virtualmachine.metadata.RInstance;
import virtualmachine.reference.RReference;

public class RInstructionSETFIELD extends RInstruction {

    private ConstantPool constantPool;

    public RInstructionSETFIELD(Stack<RReference> dataStack, Stack<ExecFrame> execStack, Heap heap, ConstantPool constantPool) {
        super(dataStack, execStack, heap);
        this.constantPool = constantPool;
    }

    @Override
    public void execute() {
        // get member name from parameter
        byte[] bc = execStack.peek().getCurrentBC();
        int mi = ByteCodeOperations.getParam32(bc, execStack.peek().getEIP());
        execStack.peek().moveEIP(4);
        String mstr = constantPool.get(mi);

        // get stack data
        RReference iref = dataStack.pop();
        RReference memref = dataStack.pop();

        RInstance inst = iref.getInstance();
        inst.setMember(mstr, memref);
    }
}
