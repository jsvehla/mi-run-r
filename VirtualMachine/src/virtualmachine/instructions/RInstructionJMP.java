package virtualmachine.instructions;

import java.util.Stack;
import virtualmachine.execution.ByteCodeOperations;
import virtualmachine.execution.ExecFrame;
import virtualmachine.execution.Heap;
import virtualmachine.reference.RReference;

public class RInstructionJMP extends RInstruction {
    public RInstructionJMP(Stack<RReference> dataStack, Stack<ExecFrame> execStack, Heap heap) {
        super(dataStack, execStack, heap);
    }

    @Override
    public void execute() {
        byte[] bc = execStack.peek().getCurrentBC();
        int number = ByteCodeOperations.getParam32(bc, execStack.peek().getEIP());
        execStack.peek().moveEIP(4);
        execStack.peek().moveEIP(number);
    }

}
