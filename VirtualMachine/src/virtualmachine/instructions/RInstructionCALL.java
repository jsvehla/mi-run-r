package virtualmachine.instructions;

import java.util.Stack;
import virtualmachine.Loadable;
import virtualmachine.execution.ByteCodeOperations;
import virtualmachine.execution.ExecFrame;
import virtualmachine.execution.Heap;
import virtualmachine.loading.ConstantPool;
import virtualmachine.metadata.RMethod;
import virtualmachine.reference.RReference;

public class RInstructionCALL extends RInstruction {

    private ConstantPool constantPool;
    private Loadable virtualMachine;

    public RInstructionCALL(ConstantPool constantPool, Stack<RReference> dataStack, Stack<ExecFrame> execStack, Heap heap, Loadable virtualMachine) {
        super(dataStack, execStack, heap);
        this.constantPool = constantPool;
        this.virtualMachine = virtualMachine;
    }

    @Override
    public void execute() {
        // get methodname from parameter
        byte[] bc = execStack.peek().getCurrentBC();
        int mi = ByteCodeOperations.getParam32(bc, execStack.peek().getEIP());
        execStack.peek().moveEIP(4);

        // load method name
        String mstr = constantPool.get(mi);

        // lookup method
        RReference inref = dataStack.peek();
        RMethod m = inref.getCallClass().getMethod(mstr);

        // call method
        m.call(dataStack, execStack, heap, virtualMachine);
    }
}
