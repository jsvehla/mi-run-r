package virtualmachine.instructions;

import java.util.Stack;
import virtualmachine.execution.ExecFrame;
import virtualmachine.execution.Heap;
import virtualmachine.metadata.RInstance;
import virtualmachine.reference.RReference;
import virtualmachine.reference.RReferenceHeap;

public class RInstructionADD extends RInstruction {

    public RInstructionADD(Stack<RReference> dataStack, Stack<ExecFrame> execStack, Heap heap) {
        super(dataStack, execStack, heap);
    }

    @Override
    public void execute() {
        RReference bref = dataStack.pop();
        RReference aref = dataStack.pop();
        RInstance b = bref.getInstance();
        RInstance a = aref.getInstance();

        dataStack.add(new RReferenceHeap(heap, heap.add(a.add(b))));
    }
}
