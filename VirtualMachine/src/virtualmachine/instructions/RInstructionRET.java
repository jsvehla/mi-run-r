package virtualmachine.instructions;

import java.util.Stack;
import virtualmachine.execution.ExecFrame;
import virtualmachine.execution.Heap;
import virtualmachine.metadata.RMethod;
import virtualmachine.reference.RReference;

public class RInstructionRET extends RInstruction {

    public RInstructionRET(Stack<RReference> dataStack, Stack<ExecFrame> execStack, Heap heap) {
        super(dataStack, execStack, heap);
    }

    @Override
    public void execute() {
        RMethod method = execStack.peek().getCurrentMethod();
        method.ret(dataStack, execStack, heap);
    }
}
