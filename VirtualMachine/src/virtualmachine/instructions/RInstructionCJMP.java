package virtualmachine.instructions;

import java.util.Stack;
import virtualmachine.execution.ByteCodeOperations;
import virtualmachine.execution.ExecFrame;
import virtualmachine.execution.Heap;
import virtualmachine.metadata.RInstance;
import virtualmachine.reference.RReference;

public class RInstructionCJMP extends RInstruction {

    public RInstructionCJMP(Stack<RReference> dataStack, Stack<ExecFrame> execStack, Heap heap) {
        super(dataStack, execStack, heap);
    }

    @Override
    public void execute() {
        byte[] bc = execStack.peek().getCurrentBC();
        int jmp = ByteCodeOperations.getParam32(bc, execStack.peek().getEIP());
        execStack.peek().moveEIP(4);
        int ejmp = ByteCodeOperations.getParam32(bc, execStack.peek().getEIP());
        execStack.peek().moveEIP(4);

        RReference bref = dataStack.pop();
        RInstance bval = bref.getInstance();

        if (bval.toInt() >= 1) {
            execStack.peek().moveEIP(jmp);
        } else {
            execStack.peek().moveEIP(ejmp);
        }
    }
}
