package virtualmachine.nativelibraries;

import virtualmachine.metadata.RClass;

public class CLStatic extends RClass {
    private static CLStatic instance;

    private CLStatic(){
        super("CL::Static");
        this.addMethod(new FNPrintln());
        this.addMethod(new FNReadln());
        this.addMethod(new FNRunProgram());
        this.addMethod(new FNCreateFile());
        this.addMethod(new FNWriteToFile());
        this.addMethod(new FNReadFile());
        this.addMethod(new FNPrintlnString());
        this.addMethod(new FNRandom());
        this.addMethod(new FNSuper());
    }

    public static CLStatic getInstance(){
        if (instance == null){
            instance = new CLStatic();
        }
        return instance;
    }
}
