package virtualmachine.nativelibraries;

import java.util.Stack;
import virtualmachine.Loadable;
import virtualmachine.execution.ExecFrame;
import virtualmachine.execution.Heap;
import virtualmachine.metadata.RMethod;
import virtualmachine.metadata.internal.RInstanceArray;
import virtualmachine.reference.RReference;

public class FNRunProgram extends RMethod {

    public FNRunProgram() {
        super("FN::runProgram::2", 0, 2, null, true);
    }

    @Override
    public void call(Stack<RReference> dataStack, Stack<ExecFrame> execStack, Heap heap, Loadable virtualMachine) {
        virtualMachine.loadProgram((RInstanceArray) dataStack.pop().getInstance(), (RInstanceArray) dataStack.pop().getInstance());
    }
}
