package virtualmachine.nativelibraries;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Stack;
import virtualmachine.Loadable;
import virtualmachine.execution.ByteCodeOperations;
import virtualmachine.execution.ExecFrame;
import virtualmachine.execution.Heap;
import virtualmachine.metadata.RInstance;
import virtualmachine.metadata.RMethod;
import virtualmachine.metadata.internal.RInstanceArray;
import virtualmachine.reference.RReference;
import virtualmachine.reference.RReferenceHeap;

public class FNReadFile extends RMethod {

    public FNReadFile() {
        super("FN::readFile::1", 0, 1, null, true);
    }

    @Override
    public void call(Stack<RReference> dataStack, Stack<ExecFrame> execStack, Heap heap, Loadable virtualMachine) {
        RInstance inst = dataStack.pop().getInstance();
        String filename = ((RInstanceArray) inst).toCharString();

        List<Integer> heapPositions = new ArrayList<Integer>();

        try {
            // Open the file that is the first
            // command line parameter
            FileInputStream fstream = new FileInputStream(filename);
            // Get the object of DataInputStream
            DataInputStream in = new DataInputStream(fstream);
            BufferedReader br = new BufferedReader(new InputStreamReader(in));
            String strLine;
            //Read File Line By Line
            while ((strLine = br.readLine()) != null) {
                // Print the content on the console
                RInstanceArray rArray = ByteCodeOperations.stringToRArray(strLine, heap);
                heapPositions.add(heap.add(rArray));
            }
            //Close the input stream
            in.close();
        } catch (Exception e) {//Catch exception if any
            System.err.println("File doesnt exist");
        }

        RInstanceArray rArray = new RInstanceArray();
        rArray.setSize(heapPositions.size());

        int i = 0;
        for (Integer ref : heapPositions){
            rArray.setIndex(i, new RReferenceHeap(heap, ref));
            i++;
        }

        dataStack.add(new RReferenceHeap(heap, heap.add(rArray)));
    }
}
