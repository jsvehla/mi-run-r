package virtualmachine.nativelibraries;

import java.util.Stack;
import virtualmachine.Loadable;
import virtualmachine.execution.ExecFrame;
import virtualmachine.execution.Heap;
import virtualmachine.metadata.RMethod;
import virtualmachine.reference.RReference;
import virtualmachine.reference.RReferenceHeap;
import virtualmachine.reference.RReferenceSuper;

public class FNSuper extends RMethod {

    public FNSuper() {
        super("FN::super::1", 0, 1, null, true);
    }

    @Override
    public void call(Stack<RReference> dataStack, Stack<ExecFrame> execStack, Heap heap, Loadable virtualMachine) {
        RReference inst = dataStack.pop();
        RReferenceSuper superref = new RReferenceSuper((RReferenceHeap) inst);

        dataStack.push(superref);
    }
}
