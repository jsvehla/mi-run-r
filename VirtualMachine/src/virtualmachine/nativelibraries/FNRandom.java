package virtualmachine.nativelibraries;

import java.util.Random;
import java.util.Stack;
import virtualmachine.Loadable;
import virtualmachine.execution.ExecFrame;
import virtualmachine.execution.Heap;
import virtualmachine.metadata.RMethod;
import virtualmachine.metadata.internal.RInstanceInteger;
import virtualmachine.reference.RReference;
import virtualmachine.reference.RReferenceHeap;

public class FNRandom extends RMethod {
public FNRandom() {
        super("FN::random::1", 0, 1, null, true);
    }

    @Override
    public void call(Stack<RReference> dataStack, Stack<ExecFrame> execStack, Heap heap, Loadable virtualMachine) {
        RInstanceInteger val = (RInstanceInteger) dataStack.pop().getInstance();

        Random r = new Random();
        int randomInt = r.nextInt(val.getValue());
        val = new RInstanceInteger(randomInt);
        dataStack.add(new RReferenceHeap(heap, heap.add(val)));
    }
}
