package virtualmachine.nativelibraries;

import java.util.Stack;
import virtualmachine.Loadable;
import virtualmachine.execution.ExecFrame;
import virtualmachine.execution.Heap;
import virtualmachine.metadata.RInstance;
import virtualmachine.metadata.RMethod;
import virtualmachine.reference.RReference;

public class FNPrintln extends RMethod {

    public FNPrintln(){
        super("FN::println::1",0,1,null, true);
    }

    @Override
    public void call(Stack<RReference> dataStack, Stack<ExecFrame> execStack, Heap heap, Loadable virtualMachine) {
        RReference ref = dataStack.pop();
        RInstance inst = ref.getInstance();
        System.out.println(inst.toString());
    }
}
