package virtualmachine.nativelibraries;

import java.io.File;
import java.io.IOException;
import java.util.Stack;
import virtualmachine.Loadable;
import virtualmachine.execution.ExecFrame;
import virtualmachine.execution.Heap;
import virtualmachine.metadata.RInstance;
import virtualmachine.metadata.RMethod;
import virtualmachine.metadata.internal.RInstanceArray;
import virtualmachine.reference.RReference;

public class FNCreateFile extends RMethod {

    public FNCreateFile() {
        super("FN::createFile::1", 0, 1, null, true);
    }

    @Override
    public void call(Stack<RReference> dataStack, Stack<ExecFrame> execStack, Heap heap, Loadable virtualMachine) {
        RInstance inst = dataStack.pop().getInstance();
        String filename = ((RInstanceArray) inst).toCharString() ;

        File f;
        f = new File(filename);
        if (f.exists()){
            f.delete();
        }
        if (!f.exists()) {
            try {
                f.createNewFile();
            } catch (IOException ex) {
                System.out.println("File exists");
            }
        }
    }
}
