package virtualmachine.nativelibraries;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Stack;
import java.util.logging.Level;
import java.util.logging.Logger;
import virtualmachine.Loadable;
import virtualmachine.execution.ByteCodeOperations;
import virtualmachine.execution.ExecFrame;
import virtualmachine.execution.Heap;
import virtualmachine.metadata.RMethod;
import virtualmachine.metadata.internal.RInstanceArray;
import virtualmachine.reference.RReference;
import virtualmachine.reference.RReferenceHeap;

public class FNReadln extends RMethod {

    public FNReadln() {
        super("FN::readln::0", 0, 0, null, true);
    }

    @Override
    public void call(Stack<RReference> dataStack, Stack<ExecFrame> execStack, Heap heap, Loadable virtualMachine) {
        try {
            InputStreamReader input = new InputStreamReader(System.in);
            BufferedReader read = new BufferedReader(input);

            String text = read.readLine();
            RInstanceArray array = ByteCodeOperations.stringToRArray(text, heap);
            int arrayHeapPos = heap.add(array);
            dataStack.add(new RReferenceHeap(heap, arrayHeapPos));

        } catch (IOException ex) {
            Logger.getLogger(FNReadln.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
}
