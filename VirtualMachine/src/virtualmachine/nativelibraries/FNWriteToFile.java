package virtualmachine.nativelibraries;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.util.Stack;
import virtualmachine.Loadable;
import virtualmachine.execution.ExecFrame;
import virtualmachine.execution.Heap;
import virtualmachine.metadata.RInstance;
import virtualmachine.metadata.RMethod;
import virtualmachine.metadata.internal.RInstanceArray;
import virtualmachine.reference.RReference;

public class FNWriteToFile extends RMethod {

    public FNWriteToFile() {
        super("FN::writeToFile::2", 0, 2, null, true);
    }

    @Override
    public void call(Stack<RReference> dataStack, Stack<ExecFrame> execStack, Heap heap, Loadable virtualMachine) {
        RInstance inst = dataStack.pop().getInstance();
        String text = ((RInstanceArray) inst).toCharString() ;

        inst = dataStack.pop().getInstance();
        String filename = ((RInstanceArray) inst).toCharString() ;

        try {
            // Create file
            FileWriter fstream = new FileWriter(filename, true);
            BufferedWriter out = new BufferedWriter(fstream);
            out.write(text);
            out.write('\n');
            //Close the output stream
            out.close();
        } catch (Exception e) {//Catch exception if any
            System.err.println("File doesnt exist");
        }
    }
}
