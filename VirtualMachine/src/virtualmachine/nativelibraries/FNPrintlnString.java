package virtualmachine.nativelibraries;

import java.util.Stack;
import virtualmachine.Loadable;
import virtualmachine.execution.ExecFrame;
import virtualmachine.execution.Heap;
import virtualmachine.metadata.RInstance;
import virtualmachine.metadata.RMethod;
import virtualmachine.metadata.internal.RInstanceArray;
import virtualmachine.reference.RReference;

public class FNPrintlnString extends RMethod {
    public FNPrintlnString(){
        super("FN::printlnString::1",0,1,null, true);
    }

    @Override
    public void call(Stack<RReference> dataStack, Stack<ExecFrame> execStack, Heap heap, Loadable virtualMachine) {
        RReference ref = dataStack.pop();
        RInstance inst = ref.getInstance();
        System.out.println(((RInstanceArray)inst).toCharString());
    }
}
