package virtualmachine;

import java.util.LinkedList;
import java.util.List;
import virtualmachine.execution.ProgramExecution;
import virtualmachine.loading.programbuilder.ProgramDirector;
import virtualmachine.loading.programbuilder.ProgramFromFileBuilder;
import virtualmachine.metadata.internal.RInstanceArray;

public class VirtualMachine implements Loadable {

    private List<ProgramExecution> programs;

    public VirtualMachine() {
        this.programs = new LinkedList<ProgramExecution>();
    }

    public void loadProgram(String filename, String params) {
        try {
            ProgramDirector programDirector = new ProgramDirector(new ProgramFromFileBuilder(filename, params, this));
            programDirector.construct();
            ProgramExecution programExecution = programDirector.getProgramExecution();
            if (programs.isEmpty() == false) {
                programs.get(0).breakProcess();
            }
            programs.add(0, programExecution);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void run() {
        while (programs.isEmpty() == false) {
            ProgramExecution program = programs.get(0);
            if (program.runEntryPoint()) {
                programs.remove(0);
            }
        }
    }

    @Override
    public void loadProgram(RInstanceArray params, RInstanceArray filename) {
        try {
            loadProgram(filename.toCharString(), params.toCharString());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
