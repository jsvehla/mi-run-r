package virtualmachine.metadata;

import java.util.Stack;
import virtualmachine.Loadable;
import virtualmachine.execution.ExecFrame;
import virtualmachine.execution.Heap;
import virtualmachine.reference.RReference;

public class RMethod {

    private String name;
    private int localsCnt;
    private int paramCnt;
    private byte[] bytecode;
    private boolean isStatic;

    public RMethod(String name, int localsCnt, int paramCnt, byte[] bytecode, boolean isStatic) {
        this.name = name;
        this.localsCnt = localsCnt;
        this.paramCnt = paramCnt;
        this.bytecode = bytecode;
        this.isStatic = isStatic;
    }

    public byte[] getBC() {
        return bytecode;
    }

    public int getVarCount() {
        return localsCnt + paramCnt;
    }

    public void call(Stack<RReference> dataStack, Stack<ExecFrame> execStack, Heap heap, Loadable virtualMachine) {
        int p = 0;
        if (isStatic == false) {
            p = 1;
        }

        // create new frame
        ExecFrame fr = new ExecFrame(this);

        // store locals, self as 0
        if (isStatic == false) {
            fr.storeVar(0, dataStack.pop().getReference());
        }
        for (int i = this.paramCnt - 1; i >= p; i--) {
            fr.storeVar(i, dataStack.pop().getReference());
        }


        fr.setEBP(dataStack.size());
        execStack.push(fr);
    }

    public void ret(Stack<RReference> dataStack, Stack<ExecFrame> execStack, Heap heap) {
        // remember ret value
        RReference ret = dataStack.pop();

        // pop previous stack frame
        int ebp = execStack.peek().getEBP();
        while (dataStack.size() != ebp) {
            dataStack.pop();
        }

        // return value push
        dataStack.push(ret);

        // pop exec frame
        execStack.pop();
    }

    String getName() {
        return name;
    }
}
