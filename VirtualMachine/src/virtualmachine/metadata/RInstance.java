package virtualmachine.metadata;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import virtualmachine.metadata.internal.arithmetics.Calculator;
import virtualmachine.reference.RReference;

public abstract class RInstance {

    private RClass rclass;
    private Map<String, RReference> members;

    public RInstance(RClass classX) {
        this.rclass = classX;
        members = new HashMap<String, RReference>();
    }

    public RClass getRClass() {
        return rclass;
    }

    public void setMember(String name, RReference instance) {
        members.put(name, instance);
    }

    public RReference getMember(String name) {
        return members.get(name);
    }

    public Iterator<String> getMembersIterator() {
        return members.keySet().iterator();
    }

    public int getMemberCount() {
        return members.size();
    }

    public abstract boolean cgt(RInstance inst);

    public abstract boolean ceq(RInstance inst);

    public abstract Calculator getCalculator();

    public abstract RInstance add(RInstance inst);

    public abstract RInstance sub(RInstance inst);

    public abstract RInstance mul(RInstance b);

    public abstract RInstance div(RInstance b);

    public abstract int toInt();

}
