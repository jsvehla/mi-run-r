package virtualmachine.metadata;

import virtualmachine.metadata.internal.arithmetics.Calculator;

public class RInstanceNonnative extends RInstance {

    public RInstanceNonnative(RClass rclass) {
        super(rclass);
    }

    @Override
    public Calculator getCalculator() {
        throw new UnsupportedOperationException("No native operation on nonnative objects.");
    }

    @Override
    public RInstance add(RInstance inst) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public RInstance sub(RInstance inst) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public RInstance mul(RInstance b) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public RInstance div(RInstance b) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public boolean ceq(RInstance inst) {
        return false;
    }

    @Override
    public int toInt() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public boolean cgt(RInstance inst) {
        throw new UnsupportedOperationException("Not supported yet.");
    }
}
