package virtualmachine.metadata;

import java.util.HashMap;
import java.util.Map;

public class RClass {
    protected RClass parent;
    protected Map<String, RMethod> methods;
    protected String name;

    public RClass(String name) {
        this.name = name;
        methods = new HashMap<String, RMethod>();
    }

    public RInstance newInstance() {
        return new RInstanceNonnative(this);
    }

    public Map<String, RMethod> getMethods() {
        return methods;
    }

    public RMethod getMethod(String name) {
        RMethod rmet = methods.get(name);
        if(rmet == null){
            // no method found - try parent
            rmet = parent.getMethod(name);
        }
        return rmet;
    }

    public void addMethod(RMethod method) {
        methods.put(method.getName(), method);
    }

    public String getName() {
        return name;
    }

    public void setParent(RClass parent) {
        this.parent = parent;
    }

    public RClass getParent() {
        return parent;
    }
}
