package virtualmachine.metadata.internal;

import virtualmachine.metadata.RClass;
import virtualmachine.metadata.RInstance;

public class RClassNull extends RClass {
    private static RClassNull instance;

    private RClassNull() {
        super("CL::Null");
    }

    public static RClassNull getInstance(){
        if (instance == null){
            instance = new RClassNull();
        }

        return instance;
    }

    @Override
    public RInstance newInstance() {
        return RInstanceNULL.getInstance();
    }
}
