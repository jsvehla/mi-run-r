package virtualmachine.metadata.internal.method;

import java.util.Stack;
import virtualmachine.Loadable;
import virtualmachine.execution.ExecFrame;
import virtualmachine.execution.Heap;
import virtualmachine.metadata.RMethod;
import virtualmachine.metadata.internal.RInstanceArray;
import virtualmachine.metadata.internal.RInstanceInteger;
import virtualmachine.reference.RReference;
import virtualmachine.reference.RReferenceHeap;

public class FNIntegerToRArray extends RMethod {

    public FNIntegerToRArray() {
        super("FN::toRArray::1", 0, 0, null, false);
    }

    @Override
    public void call(Stack<RReference> dataStack, Stack<ExecFrame> execStack, Heap heap, Loadable virtualMachine) {
        // pointer to instance
        RReference iref = dataStack.pop();

        RInstanceInteger number = (RInstanceInteger) iref.getInstance();
        dataStack.add(new RReferenceHeap(heap, heap.add(number.toRArray(heap))));
    }
}
