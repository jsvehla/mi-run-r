package virtualmachine.metadata.internal.method;

import java.util.Stack;
import virtualmachine.Loadable;
import virtualmachine.execution.ExecFrame;
import virtualmachine.execution.Heap;
import virtualmachine.metadata.RMethod;
import virtualmachine.metadata.internal.RInstanceArray;
import virtualmachine.reference.RReference;
import virtualmachine.reference.RReferenceHeap;

public class FNArrayToInt extends RMethod  {

    public FNArrayToInt() {
        super("FN::arrayToInt::1", 0, 0, null, false);
    }
    @Override
    public void call(Stack<RReference> dataStack, Stack<ExecFrame> execStack, Heap heap, Loadable virtualMachine) {
        // pointer to instance
        RReference iref = dataStack.pop();

        RInstanceArray array = (RInstanceArray) iref.getInstance();
        dataStack.add(new RReferenceHeap(heap, heap.add(array.toIntFromString())));
    }

}
