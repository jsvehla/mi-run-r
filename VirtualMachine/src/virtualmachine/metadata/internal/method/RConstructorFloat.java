package virtualmachine.metadata.internal.method;

import java.util.Stack;
import virtualmachine.Loadable;
import virtualmachine.execution.ExecFrame;
import virtualmachine.execution.Heap;
import virtualmachine.metadata.RMethod;
import virtualmachine.metadata.internal.RInstanceFloat;
import virtualmachine.reference.RReference;

public class RConstructorFloat extends RMethod {

    public RConstructorFloat() {
        super("FN::Float::2", 0, 2, null, false);
    }

    @Override
    public void call(Stack<RReference> dataStack, Stack<ExecFrame> execStack, Heap heap, Loadable virtualMachine) {
        // pointer to instance
        RReference fref = dataStack.pop();

        // value to be set
        RReference val = dataStack.pop();

        RInstanceFloat RFl = (RInstanceFloat) fref.getInstance();
        RFl.setValue(Float.intBitsToFloat(val.getData()));

        // push back pointer
        dataStack.push(fref);
    }
}
