package virtualmachine.metadata.internal.method;

import java.util.Stack;
import virtualmachine.Loadable;
import virtualmachine.execution.ExecFrame;
import virtualmachine.execution.Heap;
import virtualmachine.metadata.RMethod;
import virtualmachine.metadata.internal.RInstanceInteger;
import virtualmachine.reference.RReference;

public class RConstructorInteger extends RMethod {

    public RConstructorInteger() {
        super("FN::Integer::2", 0, 2, null, false);
    }

    @Override
    public void call(Stack<RReference> dataStack, Stack<ExecFrame> execStack, Heap heap, Loadable virtualMachine) {
        // pointer to instance
        RReference intref = dataStack.pop();

        // value to be set
        RReference val = dataStack.pop();

        RInstanceInteger RInt = (RInstanceInteger) intref.getInstance();
        RInt.setValue(val.getData());   /* if reference is not data, create int with heap address to obj */

        // push back pointer
        dataStack.push(intref);
    }
}
