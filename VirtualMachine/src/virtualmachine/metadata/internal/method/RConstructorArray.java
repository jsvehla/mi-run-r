package virtualmachine.metadata.internal.method;

import java.util.Stack;
import virtualmachine.Loadable;
import virtualmachine.execution.ExecFrame;
import virtualmachine.execution.Heap;
import virtualmachine.metadata.RMethod;
import virtualmachine.metadata.internal.RInstanceArray;
import virtualmachine.reference.RReference;
import virtualmachine.reference.RReferenceHeap;

public class RConstructorArray extends RMethod {

    public RConstructorArray() {
        super("FN::Array::2", 0, 2, null, false);
    }

    @Override
    public void call(Stack<RReference> dataStack, Stack<ExecFrame> execStack, Heap heap, Loadable virtualMachine) {
        // pointer to instance
        RReference iref = dataStack.pop();

        // set size
        RReference valref = dataStack.pop();
        RInstanceArray Rarr = (RInstanceArray) iref.getInstance();
        Rarr.setSize(valref.getInstance().toInt());

        // fill with null refs
        for (int i = 0; i < valref.getInstance().toInt(); ++i) {
            Rarr.setIndex(i, new RReferenceHeap(heap, heap.getNullRef()));
        }

        // push back pointer
        dataStack.push(iref);
    }
}
