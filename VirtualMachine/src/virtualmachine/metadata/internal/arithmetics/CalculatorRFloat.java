package virtualmachine.metadata.internal.arithmetics;

import virtualmachine.metadata.RInstance;
import virtualmachine.metadata.RInstanceNonnative;
import virtualmachine.metadata.internal.RInstanceFloat;
import virtualmachine.metadata.internal.RInstanceInteger;

public class CalculatorRFloat implements Calculator {

    private RInstanceFloat base;

    public CalculatorRFloat(RInstanceFloat base) {
        this.base = base;
    }

    @Override
    public RInstance add(RInstanceInteger RInt) {
        float val = RInt.getValue() + base.getValue();
        return new RInstanceFloat(val);
    }

    @Override
    public RInstance add(RInstanceFloat RFloat) {
        float val = RFloat.getValue() + base.getValue();
        return new RInstanceFloat(val);
    }

    @Override
    public RInstance sub(RInstanceInteger RInt) {
        float val =  RInt.getValue() - base.getValue();
        return new RInstanceFloat(val);
    }

    @Override
    public RInstance sub(RInstanceFloat RFloat) {
        float val = RFloat.getValue() - base.getValue();
        return new RInstanceFloat(val);
    }

    @Override
    public RInstance mul(RInstanceFloat RFloat) {
        float val = RFloat.getValue() * base.getValue();
        return new RInstanceFloat(val);
    }

    @Override
    public RInstance mul(RInstanceInteger RInt) {
        float val = RInt.getValue() * base.getValue();
        return new RInstanceFloat(val);
    }

    @Override
    public RInstance div(RInstanceFloat RFloat) {
        float val = RFloat.getValue() / base.getValue();
        return new RInstanceFloat(val);
    }

    @Override
    public RInstance div(RInstanceInteger RInt) {
        float val = RInt.getValue() / base.getValue();
        return new RInstanceFloat(val);
    }

    @Override
    public boolean ceq(RInstanceNonnative RIntNon) {
        return false;
    }

    @Override
    public boolean ceq(RInstanceFloat RFloat) {
        return (RFloat.getValue() == this.base.getValue());
    }

    @Override
    public boolean ceq(RInstanceInteger RInt) {
        return (RInt.getValue() == this.base.getValue());
    }

    @Override
    public boolean cgt(RInstanceNonnative RIntNon) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public boolean cgt(RInstanceFloat RFloat) {
        return (RFloat.getValue() > this.base.getValue());
    }

    @Override
    public boolean cgt(RInstanceInteger RInt) {
        return (RInt.getValue() > this.base.getValue());
    }
}
