package virtualmachine.metadata.internal.arithmetics;

import virtualmachine.metadata.RInstance;
import virtualmachine.metadata.RInstanceNonnative;
import virtualmachine.metadata.internal.RInstanceFloat;
import virtualmachine.metadata.internal.RInstanceInteger;

public interface Calculator {

    public RInstance add(RInstanceInteger RInt);

    public RInstance add(RInstanceFloat RFloat);

    public RInstance sub(RInstanceInteger RInt);

    public RInstance sub(RInstanceFloat RFloat);

    public RInstance mul(RInstanceFloat RFloat);

    public RInstance mul(RInstanceInteger RInt);

    public RInstance div(RInstanceFloat RFloat);

    public RInstance div(RInstanceInteger RIntF);

    public boolean ceq(RInstanceNonnative RIntNon);

    public boolean ceq(RInstanceFloat RFloat);

    public boolean ceq(RInstanceInteger RInt);

    public boolean cgt(RInstanceNonnative RIntNon);

    public boolean cgt(RInstanceFloat RFloat);

    public boolean cgt(RInstanceInteger RInt);
}
