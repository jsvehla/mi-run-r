package virtualmachine.metadata.internal;

import virtualmachine.metadata.RClass;
import virtualmachine.metadata.RInstance;

public class RClassFloat extends RClass {
    private static RClassFloat instance;

    private RClassFloat() {
        super("CL::Float");
    }

    public static RClassFloat getInstance(){
        if (instance == null){
            instance = new RClassFloat();
        }
        return instance;
    }

    @Override
    public RInstance newInstance() {
        return new RInstanceFloat(0);
    }
}
