package virtualmachine.metadata.internal;

import virtualmachine.metadata.RClass;
import virtualmachine.metadata.RMethod;

public class RClassBase extends RClass {
    private static RClassBase instance;

    public RClassBase() {
        super("CL::Base");
    }

    public static RClassBase getInstance() {
        if (instance == null) {
            instance = new RClassBase();
        }
        return instance;
    }

    @Override
    public RClass getParent() {
        return this;
    }

    @Override
    public RMethod getMethod(String name) {
        RMethod rmet = methods.get(name);
        if (rmet == null) {
            throw new RuntimeException("No method found");
        }
        return rmet;
    }
}
