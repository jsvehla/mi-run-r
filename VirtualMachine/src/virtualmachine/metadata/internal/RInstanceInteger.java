package virtualmachine.metadata.internal;

import virtualmachine.execution.Heap;
import virtualmachine.metadata.RInstance;
import virtualmachine.metadata.internal.arithmetics.Calculator;
import virtualmachine.metadata.internal.arithmetics.CalculatorRInteger;
import virtualmachine.reference.RReferenceHeap;

public class RInstanceInteger extends RInstance {

    private int value;

    public RInstanceInteger(int value) {
        super(RClassInteger.getInstance());
        this.value = value;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int i) {
        this.value = i;
    }

    @Override
    public RInstance add(RInstance inst) {
        return inst.getCalculator().add(this);
    }

    @Override
    public Calculator getCalculator() {
        return new CalculatorRInteger(this);
    }

    @Override
    public RInstance sub(RInstance inst) {
        return inst.getCalculator().sub(this);
    }

    @Override
    public RInstance mul(RInstance inst) {
        return inst.getCalculator().mul(this);
    }

    @Override
    public RInstance div(RInstance inst) {
        return inst.getCalculator().div(this);
    }

    @Override
    public String toString() {
        return Integer.toString(value);
    }

    public RInstanceArray toRArray(Heap heap) {
        RInstanceArray toReturn = new RInstanceArray();
        String stringValue = Integer.toString(value);
        toReturn.setSize(stringValue.length());

        for (int i = 0; i < stringValue.length(); i++) {
            toReturn.setIndex(i, new RReferenceHeap(heap, heap.add(new RInstanceInteger(stringValue.charAt(i)))));
        }

        return toReturn;
    }

    @Override
    public boolean ceq(RInstance inst) {
        return inst.getCalculator().ceq(this);
    }

    @Override
    public int toInt() {
        return value;
    }

    @Override
    public boolean cgt(RInstance inst) {
        return inst.getCalculator().cgt(this);
    }
}
