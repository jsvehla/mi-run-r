package virtualmachine.metadata.internal;

import virtualmachine.metadata.RInstance;
import virtualmachine.metadata.internal.arithmetics.Calculator;

public class RInstanceNULL extends RInstance {

    private static RInstanceNULL instance;

    private RInstanceNULL() {
        super(RClassNull.getInstance());
    }

    public static RInstanceNULL getInstance() {
        if (instance == null) {
            instance = new RInstanceNULL();
        }
        return instance;
    }

    @Override
    public Calculator getCalculator() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public RInstance add(RInstance inst) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public RInstance sub(RInstance inst) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public RInstance mul(RInstance b) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public RInstance div(RInstance b) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public boolean ceq(RInstance inst) {
        return false;
    }

    @Override
    public int toInt() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public boolean cgt(RInstance inst) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public String toString(){
        return "NULL";
    }

}
