package virtualmachine.metadata.internal;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import virtualmachine.metadata.RInstance;
import virtualmachine.metadata.internal.arithmetics.Calculator;
import virtualmachine.reference.RReference;

public class RInstanceArray extends RInstance {

    private RReference[] heapArray;

    public RInstanceArray() {
        super(RClassArray.getInstance());
    }

    public void setSize(int size) {
        heapArray = new RReference[size];
    }

    public int getSize() {
        return heapArray.length;
    }

    public void setIndex(int index, RReference ref) {
        heapArray[index] = ref;
    }

    public RReference getIndex(int index) {
        return heapArray[index];
    }

    public RInstanceInteger toIntFromString() {
        int computing = 0;
        for (int i = 0; i < heapArray.length; i++) {
            int value = ((RInstanceInteger) heapArray[i].getInstance()).getValue() - 48;
            computing += value * Math.pow(10, heapArray.length - i - 1);
        }
        return new RInstanceInteger(computing);
    }

    @Override
    public RReference getMember(String name) {
        int index = Integer.parseInt(name);
        return heapArray[index];
    }

    @Override
    public Iterator<String> getMembersIterator() {
        Map<String, RReference> all = new HashMap<String, RReference>();
        if (heapArray == null) {
            return all.keySet().iterator();
        }
        for (int i = 0; i < heapArray.length; i++) {
            all.put(String.valueOf(i), heapArray[i]);
        }

        return all.keySet().iterator();
    }

    @Override
    public boolean ceq(RInstance inst) {
        return false;
    }

    @Override
    public Calculator getCalculator() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public RInstance add(RInstance inst) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public RInstance sub(RInstance inst) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public RInstance mul(RInstance b) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public RInstance div(RInstance b) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public int toInt() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public boolean cgt(RInstance inst) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public String toString() {
        String toReturn = "";
        for (int i = 0; i < heapArray.length; i++) {
            toReturn += heapArray[i].getInstance().toString();
        }
        return toReturn;
    }

    public String toCharString() {
        String toReturn = "";
        for (int i = 0; i < heapArray.length; i++) {
            toReturn += (char) ((RInstanceInteger) heapArray[i].getInstance()).getValue();
        }
        return toReturn;
    }
}
