package virtualmachine.metadata.internal;

import virtualmachine.metadata.RClass;
import virtualmachine.metadata.RInstance;
import virtualmachine.metadata.internal.method.FNArrayLength;
import virtualmachine.metadata.internal.method.FNArrayToInt;

public class RClassArray extends RClass {
    private static RClassArray instance;

    private RClassArray() {
        super("CL::Array");
    }

    public static RClassArray getInstance(){
        if (instance == null){
            instance = new RClassArray();
            instance.addMethod(new FNArrayLength());
            instance.addMethod(new FNArrayToInt());
        }

        return instance;
    }

    public RInstance newInstance() {
        return new RInstanceArray();
    }

}
