package virtualmachine.metadata.internal;

import virtualmachine.metadata.RClass;
import virtualmachine.metadata.RInstance;
import virtualmachine.metadata.internal.method.FNIntegerToRArray;

public class RClassInteger extends RClass {
    private static RClassInteger instance;
    private RClassInteger() {
        super("CL::Integer");
    }

    public static RClassInteger getInstance(){
        if (instance == null){
            instance = new RClassInteger();
            instance.addMethod(new FNIntegerToRArray());
        }
        return instance;
    }

    @Override
    public RInstance newInstance() {
        return new RInstanceInteger(0);
    }
}
