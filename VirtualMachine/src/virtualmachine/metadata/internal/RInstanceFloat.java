package virtualmachine.metadata.internal;

import virtualmachine.metadata.RInstance;
import virtualmachine.metadata.internal.arithmetics.Calculator;
import virtualmachine.metadata.internal.arithmetics.CalculatorRFloat;

public class RInstanceFloat extends RInstance {

    private float value;

    public RInstanceFloat(float value) {
        super(RClassFloat.getInstance());
        this.value = value;
    }

    public float getValue() {
        return value;
    }

    public void setValue(float val) {
        value = val;
    }

    @Override
    public Calculator getCalculator() {
        return new CalculatorRFloat(this);
    }

    @Override
    public RInstance add(RInstance inst) {
        return inst.getCalculator().add(this);
    }

    @Override
    public RInstance sub(RInstance inst) {
        return inst.getCalculator().sub(this);
    }

    @Override
    public RInstance mul(RInstance inst) {
        return inst.getCalculator().mul(this);
    }

    @Override
    public RInstance div(RInstance inst) {
        return inst.getCalculator().div(this);
    }

    @Override
    public boolean ceq(RInstance inst) {
        return inst.getCalculator().ceq(this);
    }

    @Override
    public int toInt() {
        return (int) value;
    }

    @Override
    public boolean cgt(RInstance inst) {
        throw new UnsupportedOperationException("Not supported yet.");
    }
}
