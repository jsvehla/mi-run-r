package virtualmachine.reference;

import virtualmachine.metadata.RClass;
import virtualmachine.metadata.RInstance;

public class RReferenceSuper extends RReference {

    protected RReferenceHeap realInst;

    public RReferenceSuper(RReferenceHeap realInst) {
        super(realInst.heap, realInst.ref32);
        this.realInst = realInst;
    }

    @Override
    public RReference getReference() {
        return realInst;
    }

    @Override
    public RClass getCallClass() {
        return heap.get(ref32).getRClass().getParent();
    }

    @Override
    public boolean isHeapPointer() {
        return true;
    }

    @Override
    public RInstance getInstance() {
        return heap.get(ref32);

    }
}
