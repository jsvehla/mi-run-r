package virtualmachine.reference;

import virtualmachine.execution.Heap;
import virtualmachine.metadata.RClass;
import virtualmachine.metadata.RInstance;

public class RReferenceData extends RReference {

    public RReferenceData(Heap heap, int ref32) {
        super(heap, ref32);
    }

    @Override
    public RClass getCallClass() {
        return null;
    }

    @Override
    public boolean isHeapPointer() {
        return false;
    }

    @Override
    public RInstance getInstance() {
        return null;
    }
}
