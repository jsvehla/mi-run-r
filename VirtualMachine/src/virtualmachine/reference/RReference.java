package virtualmachine.reference;

import virtualmachine.execution.Heap;
import virtualmachine.metadata.RClass;
import virtualmachine.metadata.RInstance;

public abstract class RReference {

    protected Heap heap;
    protected int ref32;

    public RReference(Heap heap, int ref32) {
        this.heap = heap;
        this.ref32 = ref32;
    }

    public int getData() {
        return ref32;
    }

    public RReference getReference() {
        return this;
    }

    public abstract RInstance getInstance();

    public abstract RClass getCallClass();

    public abstract boolean isHeapPointer();
}
