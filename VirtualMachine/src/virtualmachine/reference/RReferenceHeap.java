package virtualmachine.reference;

import virtualmachine.execution.Heap;
import virtualmachine.metadata.RClass;
import virtualmachine.metadata.RInstance;

public class RReferenceHeap extends RReference {

    public RReferenceHeap(Heap heap, int ref32) {
        super(heap, ref32);
    }

    @Override
    public RClass getCallClass() {
        return heap.get(ref32).getRClass();
    }

    @Override
    public boolean isHeapPointer() {
        return true;
    }

    @Override
    public RInstance getInstance() {
        return heap.get(ref32);

    }
}
