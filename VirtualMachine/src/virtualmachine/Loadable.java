package virtualmachine;

import virtualmachine.metadata.internal.RInstanceArray;

public interface Loadable {
    public void loadProgram(String filename, String params);
    public void loadProgram(RInstanceArray filename, RInstanceArray params);
}
