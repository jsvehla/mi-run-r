package virtualmachine.loading;

import java.util.HashMap;
import java.util.Map;

public class ConstantPool {

    private Map<Integer, String> constants;

    public ConstantPool() {
        constants = new HashMap<Integer, String>();
    }

    public void put(String cname, int cint) {
        constants.put(cint, cname);
    }

    public String get(int cint) {
        return constants.get(cint);
    }
}
