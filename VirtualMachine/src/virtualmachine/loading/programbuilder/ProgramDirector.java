package virtualmachine.loading.programbuilder;

import virtualmachine.execution.ProgramExecution;

public class ProgramDirector {
    private ProgramBuilder programBuilder;

    public ProgramExecution getProgramExecution() {
        return programBuilder.getProgramExecution();
    }

    public ProgramDirector(ProgramBuilder programBuilder) {
        this.programBuilder = programBuilder;
    }

    public void construct(){
        programBuilder.loadData();
        programBuilder.loadBegin();
        programBuilder.loadConstantPool();
        programBuilder.loadClasses();
        programBuilder.loadMainClass();
    }
}
