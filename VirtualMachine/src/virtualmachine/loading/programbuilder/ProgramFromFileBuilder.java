package virtualmachine.loading.programbuilder;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import virtualmachine.Loadable;
import virtualmachine.execution.ByteCodeOperations;
import virtualmachine.execution.ProgramExecution;
import virtualmachine.loading.ClassPool;
import virtualmachine.loading.ProgramLoading;
import virtualmachine.metadata.RClass;
import virtualmachine.metadata.RMethod;

public class ProgramFromFileBuilder implements ProgramBuilder {

    private String filename;
    private ProgramExecution programExecution;
    private byte[] bc;
    int pos;

    public ProgramFromFileBuilder(String filename, String params, Loadable virtualMachine) {
        this.filename = filename;
        this.programExecution = new ProgramExecution(params, virtualMachine);
        pos = 0;
    }

    @Override
    public void loadData() {
        bc = ProgramLoading.load(filename);
    }

    @Override
    public void loadClasses() {
        Map<String, String> parents = new HashMap<String, String>();

        while ((pos < bc.length) && (ByteCodeOperations.sameStrings(bc, ByteCodeOperations.className, pos))) {
            pos += ByteCodeOperations.className.length();
            int id = ByteCodeOperations.getParam32(bc, pos);
            pos += 4;
            int parentID = ByteCodeOperations.getParam32(bc, pos);
            String parentName = programExecution.getConstantPool().get(parentID);
            pos += 4;
            String className = programExecution.getConstantPool().get(id);
            RClass rClass = new RClass(className);
            programExecution.getClassPool().putRClass(rClass);
            parents.put(className, parentName);

            while ((pos < bc.length) && (ByteCodeOperations.sameStrings(bc, ByteCodeOperations.methodName, pos) || ByteCodeOperations.sameStrings(bc, ByteCodeOperations.methodStaticName, pos))) {
                boolean isStatic = false;
                if (ByteCodeOperations.sameStrings(bc, ByteCodeOperations.methodName, pos)) {
                    pos += ByteCodeOperations.methodName.length();
                } else {
                    isStatic = true;
                    pos += ByteCodeOperations.methodStaticName.length();
                }

                int methodId = ByteCodeOperations.getParam32(bc, pos);
                String methodName = programExecution.getConstantPool().get(methodId);
                pos += 4;
                short paramCount = ByteCodeOperations.getParam16(bc, pos);
                pos += 2;
                short localCount = ByteCodeOperations.getParam16(bc, pos);
                pos += 2;
                int bcLength = ByteCodeOperations.getParam32(bc, pos);
                pos += 4;
                byte dst[] = new byte[bcLength];
                System.arraycopy(bc, pos, dst, 0, bcLength);

                RMethod rMethod = new RMethod(methodName, localCount, paramCount, dst, isStatic);
                if (isStatic) {
                    programExecution.getClassPool().getRClassByName("CL::Static").addMethod(rMethod);
                } else {
                    rClass.addMethod(rMethod);
                }

                pos += bcLength;
            }
        }


        ClassPool cp = programExecution.getClassPool();
        for (String className : parents.keySet()) {
            String parentName = parents.get(className);
            cp.getRClassByName(className).setParent(cp.getRClassByName(parentName));
        }
    }

    @Override
    public ProgramExecution getProgramExecution() {
        return programExecution;
    }

    @Override
    public void loadConstantPool() {
        int size = ByteCodeOperations.getParam32(bc, pos);
        pos += 4;
        int start = pos;
        while (pos < size + start) {
            int id = ByteCodeOperations.getParam32(bc, pos);
            pos += 4;
            String name = ByteCodeOperations.readString(bc, pos);
            pos += name.length();

            programExecution.getConstantPool().put(name, id);

            ++pos;
        }
    }

    @Override
    public void loadBegin() {
        if (ByteCodeOperations.sameStrings(bc, ByteCodeOperations.beginning, pos)) {
            pos += ByteCodeOperations.beginning.length();
        }
        if (ByteCodeOperations.sameStrings(bc, ByteCodeOperations.afterBeginning, pos)) {
            pos += ByteCodeOperations.afterBeginning.length();
        }
    }

    @Override
    public void loadMainClass() {
        ClassPool classPool = programExecution.getClassPool();
        Iterator<String> it = classPool.getClasses().keySet().iterator();
        while (it.hasNext()) {
            String cs = it.next();
            RClass rClass = classPool.getClasses().get(cs);
            Iterator<String> it2 = rClass.getMethods().keySet().iterator();
            while (it2.hasNext()) {
                String method = it2.next();
                if (method.compareTo("FN::Main::1") == 0) {
                    programExecution.setEntryPoint(rClass.getMethods().get(method));
                    return;
                }
            }
        }
    }
}
