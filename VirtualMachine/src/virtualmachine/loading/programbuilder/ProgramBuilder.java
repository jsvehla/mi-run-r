package virtualmachine.loading.programbuilder;

import virtualmachine.execution.ProgramExecution;

public interface ProgramBuilder {

    public ProgramExecution getProgramExecution();

    public void loadData();

    public void loadConstantPool();

    public void loadBegin();

    public void loadClasses();

    public void loadMainClass();
}
