package virtualmachine.loading.programbuilder;

import virtualmachine.execution.ProgramExecution;
import virtualmachine.loading.ConstantPool;
import virtualmachine.metadata.RMethod;

public class ProgramTestBuilder implements ProgramBuilder {
    private ProgramExecution programExecution;

    private int option;

    public ProgramTestBuilder(int option) {
        this.option = option;
        programExecution = new ProgramExecution("", null);
    }

    @Override
    public void loadData() {
        if (option == 0) {

            byte[] test = new byte[]{
                0x55, 0x00, 0x00, 0x00, 0x0A, // push 10
                0x40, 0x00, 0x00, 0x00, 0x00, // new int
                0x70, 0x00, 0x00, 0x00, 0x01, // call fn::init::1
                0x55, 0x00, 0x00, 0x00, 0x0C, // push 12
                0x40, 0x00, 0x00, 0x00, 0x00, // new int
                0x70, 0x00, 0x00, 0x00, 0x01, // call fn::init::1
                0x21, // sub
                0x71 // ret
            };

            // constant pool
            ConstantPool constantPool = programExecution.getConstantPool();
            constantPool.put("CL::Integer", 0);
            constantPool.put("FN::Init::1", 1);

            RMethod main = new RMethod("FN::Main", 0, 0, test, true);
            programExecution.setEntryPoint(main);
        }

        if (option == 1) {
        }
    }

    @Override
    public void loadClasses() {
    }

    @Override
    public ProgramExecution getProgramExecution() {
        return programExecution;
    }

    @Override
    public void loadConstantPool() {
    }

    @Override
    public void loadBegin() {
    }

    @Override
    public void loadMainClass() {
    }
}
