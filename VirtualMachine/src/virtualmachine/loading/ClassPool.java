package virtualmachine.loading;

import java.util.HashMap;
import java.util.Map;
import virtualmachine.metadata.RClass;

public class ClassPool {
    private Map<String, RClass> classes;

    public ClassPool() {
        classes = new HashMap<String, RClass>();
    }

    public void putRClass(RClass cl) {
        classes.put(cl.getName(), cl);
    }

    public RClass getRClassByName(String name) {
        return classes.get(name);
    }

    public Map<String, RClass> getClasses() {
        return classes;
    }
    

    public RClass getIntegerClass() {
        return classes.get("CL::Integer");
    }

    public RClass getFloatClass() {
        return classes.get("CL::Float");
    }

    public RClass getNullClass() {
        return classes.get("CL::Null");
    }
}
