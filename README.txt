** MI-RUN FIT CVUT **
Jan Svehla, Libor Vilimek
svehlja3, vilimlib

** Folder structure
---
Compiler
/classes		- custom classes for JavaCC
/javacc-run		- NB project files
/build.xml		- ant file for compiling compiler
/dist			- target folder for compiled jar package
/rcompiler.jjt	- grammar file

VirtualMachine	
/nbproject 		- NB project files
/src			- source files
/build.xml		- ant file for compiling VM

Knapsack				- contains application src files for knapsack solver
/combination.rrc		
/knapsackGenerator.rrc	- simple random generator for knapsack instance
/knapsackSolver.rrc		
/solver.rrc
/test-input01
/test-input02
/test-output01
/test-output02

Examples				- contains simple examples showing off interesting or common behavior (crashes are deliberate to show invalid behavior)
/array.rrc				- example creation and use of arrays and simple arithmetic
/inheritance.rrc		- example use of inheritance and method overloading
/local.rrc				- showcase of wrong swap method
/loops.rrc				- simple loop using recursion and conditions
/super.rrc				- example use of call to super class

INSTRUCTION_SET.txt		- instruction set overview
README.txt				- this file
/build.xml 				- ant file for compiling and running the knapsack solver with VM
---

** Knapsack solver
The chosen program was KnapsackSolver. Input file format is specified as : 
<num of items>
<capacity of the knapsack>
<price of first item>
<weight of first item>
...
<price of last item>
<weight of last item>
Solver can be run from command line by running "java -jar <knapsack jar> <input file name>. Solver will output short solution to cmdline and complete solution with weight and price into file "output". Format of output file is:
<binary representation of knapsack solution>
<price sum of all items in knapsack>
<weight sum of all items in knapsack>

The file can also be generated by simple random generator implemented in our language. For convenience we included two test instances with solutions.

** Knapsack generator
for ease of use and showcase, we wrote a very simple generator. Input parameter of generator is two-digit number specifying number of items generated. The output is written to file "input" in previously specified format for solver.

** Running knapsack
Knapsack can be run simply by using ant on top level build.xml. It calls subproject build files, builds VM and compiler and puts the result into folder dist/. For one-click run use 'ant run', which will build tools, compile knapsack solver and generator, use generator to generate random input file and then solve it with solver.

NOTE: do not forget to change patch to JavaCC in file Compiler/build.xml or the build will fail.

** Compiling tools
Both compiler and VM is written in Java. Ant file was provided to allow for easy compiling from command line. Compiler's ant file includes targets all, clean, compile, run, jar. Before compiling compiler path to javacc bin directory needs to be specified as property ${javacc.home} at the 3rd line of the ant file. VM's ant file was auto generated by NB and includes all common targets including clean and jar. Both applications can be compiled into jar by running "ant jar" in cmd and generated jar file is then placed into directory /dist of appropriate folder.

** Short example of class definition:
class MyClass : Base {
 fun Foo( var a ){
	var b  =(+ 10 a);
	return b;
 }
} 

** Compiler Implementation
Compiler was implemented in JavaCC. Grammar is included in file rcompiler.jjt. Language was designed as weakly typed with dynamic objects. 

** Language description
The language supports these constructs:

* Internal datatypes
Internal datatypes are as follows: Integer, float, array. Null is also implemented as internal datatype for convenience of use. Strings are emulated as arrays of 32bit data (integer) and boolean values as integers. Any values != 0 is considered as false by condition operators.

* Arithmetic Expressions
Basic arithmetics expression are supported. This includes + - * /. Expression grammar follow Polish notation with brackets. Example: (+ (-  20 10) (* 2 2)) evaluates to 24. This is a rule for all builtin math operators, including comparisons.

* Comparison
The comparison operators follow same rules as arithmetic expressions. Example (== 10 10), (> 10 2). Implemented operators are '<' '==' '>'.

* Local variables
All local variables must be declared before use by keyword 'var'. Use of undeclared variable is forbidden, same as declaring variable multiple times. Function parameters are considered declared in function header.

* Classes
All classes must explicitly declare their parent as other user class or declare 'Base' as their parent. Class definition includes functions and global functions. Class does not include members declaration and by design any class can include any number of members.

The format of class definition is:
'class' <classname> ':' <parent> {
 <class body>
}';'

* Functions
Class body is constructed by functions. Implemented are two types of functions - instance and static. Instance functions are defined by keyword 'fun', static functions with 'sfun'. Static functions are not bound to any class at runtime and multiple definitions are overridden.
Example of class with functions:

class MyClass : Base {
fun foo( var functionParam ) {
}
sfun bar( ) {
}};

* Loops
Compiler does not support for loops. All loops can be implemented as recursion calls with 'if' condition

* Conditions
Implemented condition is 'if-else' block. Else block is not required. The example of condition: if((== 10 a)) { do something } else { do something else }.

* Objects
Language supports creation of new objects. To create new object, user has to use 'new' keyword. Example: var cl = new Class(10);
Operator new first creates empty instance and then calls constructor with given number of parameters.

By design constructors are not differentiated from common methods, but must follow special rules. All constructors must end with 'return self' to return reference to object. This is by design to allow user for easy reinitialization of objects. Superclass constructors are not called and user must explicitly use @super() function to call a parent's constructors.

* Calling functions
Function calls follow common rules. Member functions are called with '.' operator (instance.foo( 10, 10)) and can be chained. Static functions are called with '@' preceding the call (@bar()).

* Members
Results of all expressions are objects. All objects can have specified members. To read a member, user use '::' operator to access the member from an instance, for example 'var a = instance::member'. To write a value to the member user must use ':::' operator in similar way: 'instance:::member = 10'. By default reading all unassigned members returns reference to null instance.

* Arrays
Similarly to members, accessing uninitialized position in array returns null. If user read index of out boundary of array. Java exception is thrown.

** Generated classfile
Format of class file is simple. First written is string 'MIRUN' then constant pool header ('CPOOL' [32bit sizeof cpool]) and constants as pair <int><string>. After the constant pool follows class definitions. Class header includes string 'class' and references to constant pool to class name and parent's class name. Then follows function definitions. Functions are recognized as static ('STATICFUNCTION') or as member functions ('FUNCTION'). Function header includes number of local variables, number of parameters (including self pointer if not static), CP reference to function name and size of generated bytecode.

** Runtime implementation
The runtime is implemented as bc interpreter with operations described in INSTRUCTION_SET.txt. First after the start of program, the internal metadata is populated by internal types and builtin methods.

The VM has two stacks. One data stack with only references to heap or raw data (references are tagged). Second stack 'Execution Stack' includes data about current method, references to parameters, EPB and EIP. 

Classes are implemented as objects with map of members and method table. Method lookup works by first reading the instance pointer from the stack after a call and getting it's class, then by looking into method table defined inside the class. If the method is not found, following the Chain of responsibility DP the search is directed to the parent class. If the method is found, new frame is created on execution stack by copying parameters into it, setting EBP and loading method's bytecode from method table. For user classes, the creation of new frame is implemented in RMethod.call() and cleaning up in RMethod.ret(). Everytime a function is called the call() is called and after return ret() is called. For internal functions this procedure is used to do a implemented behavior instead of creating new frame. As such internal methods do not differ in execution from user methods and their metadata follows same principles.

** GC
Garbage collector is implemented as simple mark-and-sweep stop-world collector. The internal implementation recursively build a tree structure (serves as marked pool) of instances referenced from datastack and local variables. Any instance added to tree is removed from toBeDeleted list (marked) and after all bound references are checked, instances remaining in this array are removed from heap.

The criteria for GC run is set not to amount of memory but number of allocated objects since last run. This decision was based on imprecise object size calculations in Java and that the number of objects is limited by number of addresses in heap.
